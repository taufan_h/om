<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of template
 *
 * @author novi-PC
 */
class template {
    //put your code here
    protected $_ci;
    function __construct() {
        $this->_ci = &get_instance();
    }
    
    
    function front($template, $data = null)
    {
        $data['content']= $this->_ci->load->view($template, $data, true);
        $data['navbar']= $this->_ci->load->view('template/navbar', $data, true);    
        $data['header']= $this->_ci->load->view('template/header', $data, true);      
        $data['footer']= $this->_ci->load->view('template/footer', $data, true);
        $this->_ci->load->view('template/template',$data);
        
    }
  
}
