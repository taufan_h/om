<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Tree {

    var $table = '';
    var $client_id = '';
    var $CI = '';
    var $ul_class = '';
    var $q = '';
    var $iteration_number = 0;
    function Tree($config = array()) {
        $this -> table = $config['table'];
        $this -> client_id = $config['client_id'];
        $this -> CI = &get_instance();
        $this -> CI -> load -> database();
        $this -> ul_class = $config['ul_class'];
    }

    function getTree($id = 1) {
        $tree = '';

        $this -> CI -> db -> where('category_id', $id);
        $this -> CI -> db -> where('client_id', $this -> client_id);
        $a = $this -> CI -> db -> get($this -> table)->row();

        $this -> CI -> db -> where('category_id', $a->category_parent_id);
        $this -> CI -> db -> where('client_id', $this -> client_id);
        $a2 = $this -> CI -> db -> get($this -> table);
        
        $num_rows = $a2->num_rows();
        $parent = $a2->row();

        if ($parent->category_parent_id != 0) {
            $tree .= $this -> getTree($parent -> category_id);
        } 
        
        if ($this->q != $parent -> category_name) {
            # code...$parent -> category_name
         $tree .= $parent -> category_name;
        }else
        {
            $tree .= '';
        }

        $tree .= " -> ".$a -> category_name;
        $this->q = $a->category_name;
           
        return $tree;
    }
}
 ?>