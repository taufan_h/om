<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
// ROUTE NYA 		ALAMAT_ROUTE = 	'NAMA_CONTROLLER/NAMA-FUNGSI/PARAM_1/PARAM_2/PARAM_N';
$route['default_controller'] = 'login/login';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['app/upload/(:any)'] = 'uploads/upload/$1';
$route['login/proses'] = 'login/login_proccess';

// ROUTE FOR DASHBOARD
$route['app/dashboard']     = 'dashboard';
$route['app/main']     = 'dashboard/main';
$route['app/logout']        = 'login/logout';
// ROUTE FOR USERS
$route['app/pengguna']             = 'user/view_user';
$route['app/pengguna_json']             = 'user/view_user_json';
$route['app/pengguna/(:num)']      = 'user/view_user/$1'; // paging
$route['app/pengguna/add']              = 'user/user_add';
$route['app/pengguna/add/proccess']     = 'user/add_proccess';
$route['app/pengguna/edit/(:num)']      = 'user/user_edit/$1';
$route['app/pengguna/edit/proccess']    = 'user/edit_proccess';
$route['app/pengguna/change_status/(:num)']         = 'user/change_status/$1'; 
$route['app/pengguna/download']              = 'user/xls_user/';
$route['app/pengguna/search']              = 'user/user_search/';
$route['app/pengguna/search/(:num)']              = 'user/user_search/$1';



// ROUTE FOR MITRA
$route['app/mitra']             = 'mitra/view_mitra';
$route['app/mitra_json']             = 'mitra/view_mitra_json';
$route['app/mitra/(:num)']      = 'mitra/view_mitra/$1'; // paging
$route['app/mitra/add/proccess']     = 'mitra/add_proccess';
$route['app/mitra/edit/(:num)']      = 'mitra/edit/$1';
$route['app/mitra/edit/proccess']    = 'mitra/edit_proccess';
$route['app/mitra/change_status/(:num)']         = 'mitra/change_status/$1'; 
$route['app/mitra/download']              = 'mitra/xls/';
$route['app/mitra/search']              = 'mitra/mitra_search/';
$route['app/mitra/search/(:num)']              = 'mitra/mitra_search/$1';


// ROUTE FOR REGION
$route['app/area']             = 'area/area_view';
$route['app/area/(:num)']      = 'area/area_view/$1'; // paging
$route['app/area/add/proccess']     = 'area/add_proccess';
$route['app/area/edit/(:num)']      = 'area/edit/$1';
$route['app/area/edit/proccess']    = 'area/edit_proccess';
$route['app/area/change_status/(:num)']         = 'area/change_status/$1'; 
$route['app/area/download']              = 'area/xls/';
$route['app/area/search']              = 'area/search/';
$route['app/area/search/(:num)']              = 'area/search/$1';


$route['app/profil']             = 'profil/profil_view';
$route['app/profil/upload']             = 'profil/upload_photo';
$route['app/profil/password']             = 'profil/change_password';
$route['app/profil/username']             = 'profil/change_username';


// ROUTE FOR PANTAUAN
$route['app/pantauan']             = 'pantauan/pantauan_view';
$route['app/pantauan/(:num)']      = 'area/pantauan_view/$1'; // paging
$route['app/pantauan/search']              = 'pantauan/search/';
$route['app/pantauan/search/(:num)']              = 'pantauan/search/$1';


// ROUTE FOR PANTAUAN
$route['app/pantauan/detail/(:any)']             = 'pantauan_detail/pantauan_view/$1';
$route['app/pantauan/detail/(:any)/(:num)']             = 'pantauan_detail/pantauan_view/$1/$2'; //paging



// ROUTE FOR REGION
$route['app/warehouse']             = 'warehouse/warehouse_view';
$route['app/warehouse/(:num)']      = 'warehouse/warehouse_view/$1'; // paging
$route['app/warehouse/add/proccess']     = 'warehouse/add_proccess';
$route['app/warehouse/edit/(:num)']      = 'warehouse/edit/$1';
$route['app/warehouse/edit/proccess']    = 'warehouse/edit_proccess';
$route['app/warehouse/change_status/(:num)']         = 'warehouse/change_status/$1'; 
$route['app/warehouse/download']              = 'warehouse/xls/';
$route['app/warehouse/search']              = 'warehouse/search/';
$route['app/warehouse/search/(:num)']              = 'warehouse/search/$1';


// ROUTE FOR REGION
$route['app/merk']             = 'merk/merk_view';
$route['app/merk/(:num)']      = 'merk/merk_view/$1'; // paging
$route['app/brand/add/proccess']     = 'merk/add_proccess';
$route['app/brand/edit/(:num)']      = 'merk/edit/$1';
$route['app/brand/edit/proccess']    = 'merk/edit_proccess';
$route['app/brand/change_status/(:num)']         = 'merk/change_status/$1'; 
$route['app/brand/download']              = 'merk/xls/';
$route['app/brand/search']              = 'merk/search/';
$route['app/brand/search/(:num)']              = 'merk/search/$1';

// ROUTE FOR REGION
$route['app/kategori']             = 'kategori/kategori_view';
$route['app/kategori/(:num)']      = 'kategori/kategori_view/$1'; // paging
$route['app/kategori/add/proccess']     = 'kategori/add_proccess';
$route['app/kategori/edit/(:num)']      = 'kategori/edit/$1';
$route['app/kategori/edit/proccess']    = 'kategori/edit_proccess';
$route['app/kategori/change_status/(:num)']         = 'kategori/change_status/$1'; 
$route['app/kategori/download']              = 'kategori/xls/';
$route['app/kategori/search']              = 'kategori/search/';
$route['app/kategori/search/(:num)']              = 'kategori/search/$1';


// ROUTE FOR BARANG
$route['app/barang']             = 'barang/barang_view';
$route['app/barang/(:num)']      = 'barang/barang_view/$1'; // paging
$route['app/barang/add/proccess']     = 'barang/add_proccess';
$route['app/barang/edit/(:num)']      = 'barang/edit/$1';
$route['app/barang/edit/proccess']    = 'barang/edit_proccess';
$route['app/barang/change_status/(:num)']         = 'barang/change_status/$1'; 
$route['app/barang/download']              = 'barang/xls/';
$route['app/barang/search']              = 'barang/search/';
$route['app/barang/search/(:num)']              = 'barang/search/$1';