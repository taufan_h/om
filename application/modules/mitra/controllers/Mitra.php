<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mitra extends CI_Controller {

public $client_id;
    function __construct() {
       parent::__construct();
       $this->cek_session();
       $this->load->model('m_mitra','model');
        $this->client_id   = $this->session->userdata('client_id');  
    }


 function cek_session()
    {       
        if($this->session->userdata("status_login") != true)
        {
           $this->session->set_flashdata('msg_login', 'Anda harus login terlebih dahulu.');
            redirect(base_url());
        }
    }

    function view_mitra($offset = null)
    {
        $sort_by = $this->input->get('sort_by');
        if(!$sort_by || $sort_by == '-') {$sort_by = 'mitra_name';}
        if($offset == null) {$offset = 0;}
        $limit = 20;     
        
        $data['list']      = $this->model->select_mitra($this->client_id, $sort_by, $offset, $limit)->result();
        $total_data = $this->model->total_mitra()->num_rows();   
       
        $data['region'] = $this->model->select_where('tbl_region','client_id',$this->client_id)->result();
        $data['mitra_type'] = $this->model->select_data('tbl_mitra_type')->result();
        $this->pagination->initialize($this->config_paging($limit, $total_data,"load"));
       
        $data['action'] = base_url().'app/mitra/add/proccess';
        $data['pagination'] = $this->pagination->create_links();
      
      $this->template->front('mitra/view_mitra',$data);
        
    }
  
     function mitra_search($offset = null)
    {
      $sort_by = $this->input->post('sort_by');
      $keyword = $this->input->post('keyword');

       // $sort_by = 'mitra_active_status';
       // $keyword = '';

        if(!$sort_by || $sort_by == '-') {$sort_by = 'mitra_name';}
        if($offset == null) {$offset = 0;}
        $limit = 20;
       
  
        
        $data['list']      = $this->model->select_search($sort_by, $offset, $limit, $keyword)->result();
        $total_data = $this->model->total_search($keyword)->num_rows();   
       
      $this->pagination->initialize($this->config_paging($limit, $total_data,"search"));
        $data['sort_by']       = $sort_by;
        $data['search']     = $keyword;
        $data['pagination'] = $this->pagination->create_links();
       $arr = array('RC' => 999, 'MESSAGE' =>  $data['list'], 'search' => $data['search'], 'total' => $total_data, 'paging' => $data['pagination']);
       $json = json_encode($arr);
       echo $json; 
     
        
            //$this->load->view('user/admin_view_user',$data);
        
    }
    
            
    function config_paging($limit, $total_data,$type)
    {
     
        $config['base_url']     = site_url().'app/mitra/search/';
      
        $config['total_rows']   = $total_data;
        $config['per_page']     = $limit;
        $config["uri_segment"]  = 4;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"]    = floor($choice);
        //$config['page_query_string'] = TRUE;
        //config for bootstrap pagination class integration
        $config['attributes'] = array('class' => 'page-link');
        $config['full_tag_open']    = '<ul class="pagination">';
        $config['full_tag_close']   = '</ul>';
        $config['first_link']       = false;
        $config['last_link']        = false;
        $config['first_tag_open']   = '<li class="page-item">';
        $config['first_tag_close']  = '</li>';
        $config['prev_link']        = '&laquo';
        $config['prev_tag_open']    = '<li class="prev">';
        $config['prev_tag_close']   = '</li>';
        $config['next_link']        = '&raquo';
        $config['next_tag_open']    = '<li class="page-item">';
        $config['next_tag_close']   = '</li>';
        $config['last_tag_open']    = '<li class="page-item">';
        $config['last_tag_close']   = '</li>';
        $config['cur_tag_open']     = '<li class="page-item active"><a class="page-link" href="#">';
        $config['cur_tag_close']    = '</a></li>';
        $config['num_tag_open']     = '<li>';
        $config['num_tag_close']    = '</li>';
        
        return $config;
    }
    
    function add_proccess()
    {
        
        $mitra_name           = $this->input->post('mitra_name');
        $mitra_type           = $this->input->post('mitra_type');
        $mitra_address           = $this->input->post('mitra_address');
        $mitra_email              = $this->input->post('mitra_email');
        $mitra_phone           = $this->input->post('mitra_phone');
        $mitra_region             = $this->input->post('mitra_region');
        $mitra_bill_address              = $this->input->post('mitra_bill_address');
        $mitra_delivery_Address           = $this->input->post('mitra_delivery_Address');
        $mitra_city             = $this->input->post('mitra_city');
        $mitra_npwp              = $this->input->post('mitra_npwp');
        $mitra_discount           = $this->input->post('mitra_discount');
        $mitra_batas_kredit             = $this->input->post('mitra_batas_kredit');
        $mitra_waktu_pelunasan              = $this->input->post('mitra_waktu_pelunasan');
        $mitra_active_status              = $this->input->post('mitra_active_status');

        $data = array(
            'client_id'             => $this->client_id,
            'mitra_name'         => $mitra_name,
            'mitra_type_id'              => $mitra_type,
            'mitra_address'         => $mitra_address,
            'mitra_email'              => $mitra_email,
            'mitra_created_date'     => date('Y-m-d H:i:s'),
            'mitra_phone'        => $mitra_phone,
            'mitra_region_id'    => $mitra_region,
            'mitra_bill_address'         => $mitra_bill_address,
            'mitra_delivery_address'     => $mitra_delivery_Address,
            'mitra_city'        => $mitra_city,
            'mitra_npwp'    => $mitra_npwp,
            'mitra_discount'         => $mitra_discount,
            'mitra_batas_kredit'    => $mitra_batas_kredit,
            'mitra_waktu_pelunasan'         => $mitra_waktu_pelunasan,
            'mitra_active_status'         => $mitra_active_status
        );
        $cek = $this->model->cek_nama('tbl_mitra','mitra_name',$mitra_name)->num_rows();
         if($this->getLatLng($mitra_address) == '0')
        {
        $json_e  = array('RC' =>'998','MESSAGE' => 'Isikan Alamat Lengkap', 'URL' => base_url().'app/mitra');
        }else
        {
          if($cek >0)
          {
          $json_e  = array('RC' =>'998','MESSAGE' => 'Mitra sudah ada', 'URL' => base_url().'app/mitra');
          }else{
              $insert = $this->model->insert_data('tbl_mitra',$data);  
          $json_e  = array('RC' =>'999','MESSAGE' => 'Sukses Tanmbah Data', 'URL' => base_url().'app/mitra');
          }
        }

        
        echo json_encode($json_e);
       
    }
    
    function edit($id)
    {
        $region         = $this->model->select_where('tbl_region','client_id',$this->client_id)->result();         
        $get                    = $this->model->ambil_data('tbl_mitra','mitra_id',$id)->row();
        $mitra_type   = $this->model->select_data('tbl_mitra_type')->result();
        $data['action']    = 'mitra/edit/proccess';
        //$this->load->view('user/formodel',$data);
        echo json_encode(array('RC' => '999', 'DATA' => $get,'REGION' => $region,'MITRA_TYPE' => $mitra_type ,'ACTION' => $data['action']));
    }
    function edit_proccess()
    {
         
        $mitra_name           = $this->input->post('mitra_name');
        $mitra_id              = $this->input->post('mitra_id');
        $mitra_type           = $this->input->post('mitra_type');
        $mitra_address           = $this->input->post('mitra_address');
        $mitra_email              = $this->input->post('mitra_email');
        $mitra_phone           = $this->input->post('mitra_phone');
        $mitra_region             = $this->input->post('mitra_region');
        $mitra_bill_address              = $this->input->post('mitra_bill_address');
        $mitra_delivery_address           = $this->input->post('mitra_delivery_address');
        $mitra_city             = $this->input->post('mitra_city');
        $mitra_npwp              = $this->input->post('mitra_npwp');
        $mitra_discount           = $this->input->post('mitra_discount');
        $mitra_batas_kredit             = $this->input->post('mitra_batas_kredit');
        $mitra_waktu_pelunasan              = $this->input->post('mitra_waktu_pelunasan');
        $mitra_active_status              = $this->input->post('mitra_active_status');
        
       $data = array(
            'mitra_name'         => $mitra_name,
            'mitra_type_id'              => $mitra_type,
            'mitra_address'         => $mitra_address,
            'mitra_email'              => $mitra_email,
            'mitra_phone'        => $mitra_phone,
            'mitra_region_id'    => $mitra_region,
            'mitra_bill_address'         => $mitra_bill_address,
            'mitra_delivery_address'     => $mitra_delivery_address,
            'mitra_city'        => $mitra_city,
            'mitra_npwp'    => $mitra_npwp,
            'mitra_discount'         => $mitra_discount,
            'mitra_batas_kredit'    => $mitra_batas_kredit,
            'mitra_waktu_pelunasan'         => $mitra_waktu_pelunasan,
            'mitra_active_status'         => $mitra_active_status
        );
      
        $cek = $this->model->cek_nama_not_in('tbl_mitra','mitra_name','mitra_id',$mitra_name,$mitra_id)->num_rows();
         if($this->getLatLng($mitra_address) == '0')
        {
        $json_e  = array('RC' =>'998','MESSAGE' => 'Isikan Alamat Lengkap', 'URL' => base_url().'app/mitra');
        }else
        {
            if($cek >0)
          {
          $json_e  = array('RC' =>'998','MESSAGE' => 'nama perusahaan sudah ada', 'URL' => base_url().'app/mitra');
          }else{
              $this->model->update_data('tbl_mitra','mitra_id',$mitra_id,$data);
          $json_e  = array('RC' =>'999','MESSAGE' => 'Data Berhasil diedit', 'URL' => base_url().'app/mitra');
              
          }
        }


        
        echo json_encode($json_e);
    }
    function change_status($id)
    {
        
        $status_data = $this->model->select_where('tbl_mitra','mitra_id',$id)->row();
        $status = $status_data->mitra_active_status;

        switch ($status) {
          case '1':
          $status_code = '0';
          $status_name = 'Non-Aktif';
            break;
           case '0':
          $status_code = '1';
          $status_name = 'Aktif';
            break;
        }

        $data = array(
           'mitra_active_status'     => $status_code
        );
        $this->model->update_data('tbl_mitra','mitra_id',$id,$data);
        $this->session->set_flashdata('data','Sukses ubah status.');
        
        $json_e  = array('RC' =>'999','ID' => $id,'STATUS_CODE' => $status_code,'STATUS_NAME' => $status_name, 'MESSAGE' => 'Sukses');
        echo json_encode($json_e);
    }
    
    function xls_mitra()
    {
      $tbl='User List<br><br><table border="1" width="100%" style="text-align:center;">';
      $tbl.='<tr height="30" bgcolor="#0C82E5">
        <th id="th1">No.</th>
        <th id="th1">Full Name</th>
        <th id="th1">Username</th>
        <th id="th1">Superior</th>
        <th id="th1">Status</th>
        <th id="th1">Region</th>
        <th id="th1">Created Date</th>
        <th id="th1">Last Login</th>
        <th id="th1">Aging</th>
        </tr>';           
      
      $client_id   = $this->session->userdata('client_id');
        $user_id            = $this->session->userdata('user_id');        
        $group_id           = $this->session->userdata('group_id');
        
     if($group_id == '1')
     {
          $data      = $this->model->xls_select_user($client_id, $user_id, 'username');
       
     }else
     {
          $data      = $this->model->xls_select_user_manager($client_id, $user_id, 'username'); 
       
     }
      $region         = $this->model->select_where('tbl_region','client_id',$client_id)->result();
        
      $parent_user       = $this->model->select_where('tbl_user','client_id',$client_id)->result();
       if ($data->num_rows() > 0) {
         $no = 1;
          foreach($data->result() as $value)
          {
             $region_name = "-";
             foreach ($region as $v_region) {
                if($value->region_id == $v_region->region_id)
                {
                     $region_name = $v_region->region_name;
                }
            }
            $superior = "-";
            foreach($parent_user as $v_user) {
                if($value->parent_user_id == $v_user->user_id)
                {
                     $superior = $v_user->username;
                }
            }
            if($value->active_status == 1) {$status = 'Active';}else {$status = 'Disabled';}
                                                
            if($value->active_status == '1'){$enable = "<i class='ti-check'>"; $stt = '0';}else {$enable = "<i class='ti-close'>"; $stt = '1';}
           
            $tbl.="<tr height='25'>
                  <td align='center'>".$no."</td>   
                  <td>".$value->full_name."</td>
                  <td>".$value->username."</td>
                  <td>".$superior."</td>
                  <td>".$status."</td>
                  <td>Sales ".$region_name."</td>
                  <td>".$value->user_date_in." (GMT+7)</td>
                  <td>".$value->user_last_login." (GMT+7)</td>
                  <td>".$value->selisih." Day's Inactive</td>
                 </tr>";
            $no++;
          }
       }else
       {

      $tbl .= '<tr><td colspan="8">No data</td></tr>';
       }
      

      
      $tbl .='</table><br><br><i>Created & downloaded from ZD</i>';        
      $namafile = "Users xls";
      header("Content-type: application/vnd.ms-excel");
      header("Content-Disposition: attachment; filename=\"$namafile.xls\"");
      echo $tbl;
    }
  function getLatLng($address)
  {
    $address = str_replace(' ', '-', $address);
    $url = 'https://maps.googleapis.com/maps/api/geocode/json?address='.trim($address).'&sensor=false&key='.key_googleapis;
    $json = @file_get_contents($url);
    $data=json_decode($json);
    $status = $data->status;
    if($status=="OK")
      return $data->results[0]->geometry->location->lat."#".$data->results[0]->geometry->location->lng;
    else
      return "0";
  }   

}
