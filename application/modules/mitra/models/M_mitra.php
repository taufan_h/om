<?php

class M_mitra extends CI_Model {
    //put your code here
    public $client_id;
    public $user_id;
    public function __construct() {
        parent::__construct();

        $this->user_id = $this->session->userdata('user_id');
        $this->client_id = $this->session->userdata('client_id');
    }
    
    function select_mitra($client_id, $sort_by, $offset, $limit)
    {
        $result = $this->db->query("select * FROM tbl_mitra  
                    inner join tbl_region on tbl_region.region_id = tbl_mitra.mitra_region_id 
                    inner join tbl_mitra_type using (mitra_type_id)        
                    WHERE tbl_mitra.client_id = '".$client_id."' 
                    ORDER BY ".$sort_by." asc LIMIT $offset, $limit");
        return $result;
    }
    
    function ambil_data($tbl,$field,$id)
    {
        $result = $this->db->query("select * FROM $tbl  
                    inner join tbl_region on tbl_mitra.mitra_region_id = tbl_region.region_id  
                    inner join tbl_mitra_type using (mitra_type_id) 
                    WHERE $field  = '".$id."'");
        return $result;
    }
    function select_data($table)
    {
        $this->db->select('*');
        return $this->db->get($table);
    }
    
    function total_mitra()
    {
        $result = $this->db->query("select * FROM tbl_mitra      
                    WHERE tbl_mitra.client_id = '".$this->client_id."'");
        return $result;
    }
    
    function cek_nama($table, $field, $record)
    {
        $this->db->select('*')
                ->where($field,$record)
                ->where('client_id',$this->client_id);
                
        $sql = $this->db->get($table);
        return $sql;
                
    }
     function cek_nama_not_in($table, $field_where,$field_not_id, $record,$user_id)
    {
        $this->db->select('*')
                ->where($field_where,$record)
                ->where_not_in($field_not_id,$user_id)
                ->where('client_id',$this->client_id);
                
        $sql = $this->db->get($table);
        return $sql;
                
    }
    function select_where($table, $field, $record)
    {
        $this->db->select('*')
                ->where($field,$record)
                ->from($table);
        return $this->db->get();
               
    }
    function insert_data($table,$data)
    {
        return $this->db->insert($table, $data);
    }
    function update_data($table,$field,$record,$data)
    {
        $this->db->where($field, $record);
        $sql = $this->db->update($table, $data); 
        return $sql;
    }

     function select_search($sort_by, $offset, $limit, $keyword)
    {
        $result = $this->db->query("select * FROM tbl_mitra  
                    inner join tbl_region on tbl_region.region_id = tbl_mitra.mitra_region_id 
                    inner join tbl_mitra_type using (mitra_type_id)        
                    WHERE tbl_mitra.client_id = '".$this->client_id."' && mitra_name like '%".$keyword."%' or mitra_address like '%".$keyword."%' or mitra_phone like '%".$keyword."%' ORDER BY ".$sort_by." asc LIMIT $offset, $limit");
        return $result;
    }
   
    function total_search($keyword)
    {
        $result = $this->db->query("select * FROM tbl_mitra                            
                    WHERE tbl_mitra.client_id = '".$this->client_id."'  && mitra_name like '%".$keyword."%' or mitra_address like '%".$keyword."%' or mitra_phone like '%".$keyword."%'");
        return $result;
    }
   
    
}
