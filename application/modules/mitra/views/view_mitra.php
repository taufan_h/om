 <div class="ks-column ks-page">
        <div class="ks-header">
            <section class="ks-title">
                <h3>Mitra</h3>

                <div class="ks-controls">
                    <button class="btn btn-primary-outline ks-light ks-filemanager-navigation-block-toggle" data-block-toggle=".ks-filemanager-page > .ks-navigation">Navigation</button>
                    <button class="btn btn-primary-outline ks-light ks-filemanager-info-block-toggle" data-block-toggle=".ks-filemanager-page > .ks-info">Info</button>
                </div>
            </section>
           <!-- <center><div class="message"><span class="badge badge-pill badge-success"><div class="alert" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Warning!</strong> Better check yourself, you're not looking too good.
</div></span></center>-->
        </div>

        <div class="ks-content">
            <div class="ks-body ks-filemanager-page">
                <div class="ks-navigation ks-scrollable" data-auto-height>
    <div class="ks-wrapper">
        <div class="ks-separator tambah_baru">Tambah Baru</div>
              <ul class="ks-tree">
                <li class="ks-has-submenu">
                <a>
                    <form name="F1" id="F1">
                        <div class="form-group">
                            <label for="subject">Jenis Usaha</label>
                            <select name="mitra_type" class="form-control ks-select mitra_type" id="select2-id-label-single mitra_type">
                                <?php
                                foreach ($mitra_type as $key) {
                                    # code...
                                    echo "<option value='".$key->mitra_type_id."'>".$key->mitra_type_name."</option>";
                                }
                            ?>
                            </select>
                        </div>                     
                        <div class="form-group">
                            <label for="subject">Nama Perusahaan</label>
                            <input type="text" name="mitra_name" class="form-control mitra_name" id="mitra_name" placeholder="Nama Perusahaan">
                            <input type="hidden" name="mitra_id" class="form-control" id="mitra_id">
                            <input type="hidden" name="action" class="form-control" id="action" value="<?php echo $action; ?>">
                        </div>                   
                        <div class="form-group">
                            <label for="subject">Alamat</label>
                            <input type="text" name="mitra_address" class="form-control mitra_address" id="mitra_address" placeholder="Alamat Perusahaan">
                        </div>
                        <div class="form-group">
                            <label for="subject">Kota</label>
                            <input type="text" name="mitra_city" class="form-control" id="mitra_city" placeholder="Kota">
                        </div>                   
                        <div class="form-group">
                            <label for="subject">Telepon</label>
                            <input type="text" name="mitra_phone" class="form-control mitra_phone" id="mitra_phone" placeholder="Telepon">
                        </div>
                        <div class="form-group">
                            <label for="subject">Email</label>
                            <input type="text" name="mitra_email" class="form-control mitra_email" id="mitra_email" placeholder="Email">
                        </div> 
                        <div class="form-group">
                            <label for="subject">No. NPWP</label>
                            <input type="text" name="mitra_npwp" class="form-control mitra_npwp" id="mitra_npwp" placeholder="No. NPWP">
                        </div>                           
                        <div class="form-group">
                            <label for="subject">Area</label>
                            <select name="mitra_region" class="form-control ks-select mitra_region" id="select2-id-label-single mitra_region">
                                 <?php
                                foreach ($region as $key) {
                                    # code...
                                    echo "<option value='".$key->region_id."'>".$key->region_name."</option>";
                                }
                            ?>
                            </select>
                        </div>  
                        <div class="form-group">
                            <label for="subject">Diskon (%)</label>
                            <input type="text" name="mitra_discount" class="form-control mitra_discount" id="mitra_discount" placeholder="Besaran diskon dalam angka">
                        </div>
                        <div class="form-group">
                            <label for="subject">Batas Kredit</label>
                            <input type="text" name="mitra_batas_kredit" class="form-control mitra_batas_kredit" id="mitra_batas_kredit" placeholder="Batas nominal order yang diperbolehkan">
                        </div> 
                        <div class="form-group">
                            <label for="subject">Waktu Pelunasan (hari)</label>
                            <input type="text" name="mitra_waktu_pelunasan" class="form-control mitra_waktu_pelunasan" id="mitra_waktu_pelunasan" placeholder="Ketik dalam angka">
                        </div>                                                  
                        <div class="ks-separator">&nbsp;</div>
                        <div class="ks-separator">&nbsp;</div>
                        <div class="form-group">
                            <label for="subject">Alamat Pengiriman</label>
                            <input type="text" name="mitra_delivery_Address" class="form-control mitra_delivery_Address" id="mitra_delivery_Address" placeholder="Alamat Pengiriman">
                        </div> 
                        <div class="form-group">
                            <label for="subject">Alamat Penagihan</label>
                            <input type="text" name="mitra_bill_address" class="form-control mitra_bill_address" id="mitra_bill_address" placeholder="Alamat Penagihan">
                        </div>                          
                        <div class="form-group">
                            <label for="subject">Status Aktif</label>
                            <select name="mitra_active_status" class="form-control ks-select mitra_active_status" id="select2-id-label-single mitra_active_status">
                                <option value="1">Aktif</option>
                                <option value="0">Tidak Aktif</option>
                            </select>
                        </div>                   
                        <div class="form-group">
                            <div class="ks-actions">
                                <button type="button" onclick="saveData()" class="btn btn-success">Kirim</button>
                                <input type="reset" class="btn btn-primary-outline ks-light ks-view-cancel" value="Batal"></input>
                                </button>
                            </div>
                        </div>
                    </form>
                </a>
            </li>
        </ul>
    </div>
</div>

<div class="ks-files"> 

    <div class="ks-header">

        <div class="ks-search input-icon icon-right icon icon-lg">
            <input id="input-group-icon-text" type="text" class="form-control cari" id="cari" placeholder="Cari...">
            <span class="icon-addon">
                <span class="fa fa-search" onclick="setData()"></span>
            </span>
        </div>
        <div class="ks-filters">
            <div class="ks-sortable btn-group">
                <button class="btn btn-primary-outline ks-light dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="btn-text-lighter">Urutkan</span>
                </button>
                <div class="dropdown-menu dropdown-menu-right ks-dropdown-menu-sortable urutkan">
                        <input name="sort_by" id="sort_by" type="hidden" value="mitra_name">
                    <a class="dropdown-item dropdown-item-checked" href="#" id="mitra_name" onclick="urutkan('mitra_name')">
                        <span class="ks-sort-by-text">Berdasarkan</span> Nama Usaha
                        <input name="radio" type="radio">
                    </a>
                    <a class="dropdown-item" href="#" id="region_name" onclick="urutkan('region_name')">
                        <span class="ks-sort-by-text">Berdasarkan</span> Area
                        <input name="radio" type="radio">
                    </a>
                   <!-- <a class="dropdown-item" href="#" onclick="urutkan('mitra_last_login')">
                        <span class="ks-sort-by-text">Berdasarkan</span> Terakhir Login
                        <input name="radio" type="radio">
                    </a>-->
                    <a class="dropdown-item" href="#" id="mitra_active_status" onclick="urutkan('mitra_active_status')">
                        <span class="ks-sort-by-text">Berdasarkan</span> Status Aktif
                        <input name="radio" type="radio">
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="ks-content ks-scrollable" data-auto-height data-fix-height="40">
        <!--table-->
        <div class="card-block">
            <table class="table table-bordered vertical-align-middle tabelnya" id="tabelnya">
                <thead>
                <tr>
                    <th width="110">Nama Mitra</th>
                    <th width="160">Alamat</th>
                    <th width="20">Telepon</th>
                    <th width="80">Area</th>
                    <th width="20">Diskon</th>
                    <th width="20">Batas Kredit (IDR)</th>
                    <th width="20">Waktu Pelunasan</th>
                    <th width="30">Status Aktif</th>
                    <th>&nbsp;</th>
                </tr>
                </thead>
                <tbody id="tbody">
                <?php
                foreach ($list as $key) {
                   
                    switch ($key->mitra_active_status) {
                        case '1':
                            # code...
                        $status = '<span class="badge badge-pill badge-success">Aktif</span>';
                        $status1 = "Non-Aktif";
                        $status_balik = "0";
                            break;
                        case '0':
                            # code...
                        $status = '<span class="badge badge-pill badge-danger">Non-Aktif</span>';
                        $status1 = "Aktif";
                        $status_balik = "1";
                            break;
                    }
                    # code...
                    echo '
                        <tr class="row-checked" id="tr_'.$key->mitra_id.'">
                     <td>
                        <div class="text-block">
                            <div class="text-block-container">
                                <div class="text-block-text">'.$key->mitra_name.'</div>
                                <div class="text-block-sub-text">'.$key->mitra_email.'</div>
                            </div>
                        </div>
                    </td>
                   <td>
                        <div class="table-cell-block image">
                            <div class="text-block-container">
                                <div class="text-block-text">'.$key->mitra_address.'</div>
                                <div class="text-block-sub-text">'.$key->mitra_city.'</div>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="table-cell-block">
                            <div class="text-block-container">
                                <div class="text-block-text">'.$key->mitra_phone.'</div>
                            </div>
                        </div>
                    </td>                    
                    <td>
                        <div class="table-cell-block">
                            <div class="text-block-container">
                                <div class="text-block-text">'.$key->region_name.'</div>
                            </div>
                        </div>
                    </td>  
                    <td>
                        <div class="table-cell-block">
                            <div class="text-block-container">
                                <div class="text-block-text">'.$key->mitra_discount.' %</div>
                            </div>
                        </div>
                    </td>                                      
                    <td>
                        <div class="table-cell-block">
                            <div class="text-block-container">
                                <div class="text-block-text">Rp.'.number_format($key->mitra_batas_kredit,0,',','.').'</div>
                            </div>
                        </div>
                    </td> 
                    <td>
                        <div class="table-cell-block">
                            <div class="text-block-container">
                                <div class="text-block-text">'.$key->mitra_waktu_pelunasan.' hari</div>
                            </div>
                        </div>
                    </td>            
                    <td>
                        <div class="table-cell-block status">
                            <div class="status-block-container">
                                <span class="user_active_status_'.$key->mitra_id.'">'.$status.'</span>
                            </div>
                        </div>
                    </td>                                                    
                    <td class="table-actions">
                        <div class="dropdown">
                            <a class="btn btn-link" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="fa fa-ellipsis-h"></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                                <a class="dropdown-item" href="#" onClick=edit("'.base_url().'app/mitra/edit/'.$key->mitra_id.'")>
                                    <span class="fa fa-pencil icon text-primary-on-hover"></span> Edit
                                </a>
                                <a class="dropdown-item" href="#" onClick=changeStatus("'.base_url().'app/mitra/change_status/'.$key->mitra_id.'")><span class="fa fa-remove icon text-danger-on-hover "></span><span class="aksi_user_active_status_'.$key->mitra_id.'">'.$status1.'</span></a>
                            </div>
                        </div>
                    </td>
                </tr>
                    ';
                }
                ?>               
                </tbody>
            </table>

            <!--paging-->
            <div class="ks-items-block pages_paging" id="pages_paging"><nav>
                <?php echo $pagination; ?></nav>
            </div>
            <!--end paging-->
        </div>
        <!--end--> 
    </div>
</div>
<script type="text/javascript">



function changeStatus(url)
{
    $.ajax({
    type: "GET",
    url: url,
    cache: false,                  
    success:function(response) {
        console.log(response);
        var objJSON = JSON.parse(response);
        var id = objJSON.ID;
        var status_name = objJSON.STATUS_NAME;
        if (objJSON.STATUS_CODE == '1') {
            var revert_status_name = 'Non-Aktif';
        }else
        {
            var revert_status_name = 'Aktif';
        }
        $('.aksi_user_active_status_'+id).html(revert_status_name);
        if (objJSON.STATUS_CODE == '1') {
            $('.user_active_status_'+id).html('<span class="badge badge-pill badge-success">Aktif</span>');
        }else 
        {
            $('.user_active_status_'+id).html('<span class="badge badge-pill badge-danger">Non-Aktif</span>');
        }
    }
});
}
// fungsi tambah baru
function saveData()
{
    var url = $('#action').val();
        $.ajax({
    type: "post",
    url: url,
    cache: false,               
    data: $('#F1').serialize(),    
    success:function(response) {
      //console.log(response); 
      var objJSON = JSON.parse(response); 
      if (objJSON.RC != 999) {
            alert(objJSON.MESSAGE);
      }else 
      {
        //console.log(objJSON.RC);
            alert(objJSON.MESSAGE);
        setData();
        bersihkanForm();
       // setPaging();
      }    
    
    }
}); 
}
function bersihkanForm()
{
    $('#F1').trigger('reset')
}

// untuk menampilkan data ke tabel
function setData()
{
    var cari = $('.cari').val();
    var sort_by = $('#sort_by').val();
     $.ajax({
        type: "post",
        url: '<?php echo base_url()."app/mitra/search";?>',
        data: {sort_by : sort_by, keyword : cari},
        success: function(res){
            console.log(res);
            var obj = JSON.parse(res);
        if (obj.RC == 999) {
           setContent(obj);
        }
        
        }
         
    });
 

}


function urutkan(sort_by)
{
    $('#sort_by').val(sort_by);
    $('.urutkan a').removeClass('dropdown-item-checked');
    var sort =  $('#sort_by').val();
    setData();
}
function edit(url)
{
  $('.tambah_baru').html('Edit Data');
  $.ajax({
    type: "GET",
    url: url,
    cache: false,                  
    success:function(response) {
        console.log(response);
        var objJSON = JSON.parse(response);

        var mitra_id = objJSON.DATA.mitra_id;
        var action = objJSON.ACTION;
        var mitra_name = objJSON.DATA.mitra_name;
        var mitra_address = objJSON.DATA.mitra_address;
        var active_status = objJSON.DATA.mitra_active_status;
        var mitra_delivery_address = objJSON.DATA.mitra_delivery_address;
        var mitra_bill_address = objJSON.DATA.mitra_bill_address;
        var mitra_phone = objJSON.DATA.mitra_phone;
        var mitra_type_id = objJSON.DATA.mitra_type_id;        
        var mitra_region_id = objJSON.DATA.mitra_region_id;
        var mitra_discount = objJSON.DATA.mitra_discount;
        var mitra_batas_kredit = objJSON.DATA.mitra_batas_kredit;
        var mitra_waktu_pelunasan = objJSON.DATA.mitra_waktu_pelunasan;
        var mitra_npwp = objJSON.DATA.mitra_npwp;
        var mitra_city = objJSON.DATA.mitra_city;
        var mitra_email = objJSON.DATA.mitra_email;
        var mitra_region_name = objJSON.DATA.region_name;
        var mitra_type_name = objJSON.DATA.mitra_type_name;

        $('#mitra_name').val(mitra_name);
        $('#mitra_address').val(mitra_address);        
        $('#mitra_id').val(mitra_id);       
        $('#action').val(action);
        $('#mitra_delivery_address').val(mitra_delivery_address);
        $('#mitra_bill_address').val(mitra_bill_address);
        $('#mitra_phone').val(mitra_phone);        
        $('#mitra_discount').val(mitra_discount);       
        $('#mitra_batas_kredit').val(mitra_batas_kredit);
        $('#mitra_npwp').val(mitra_npwp);
        $('#mitra_waktu_pelunasan').val(mitra_waktu_pelunasan);       
        $('#mitra_city').val(mitra_city);
        $('#mitra_email').val(mitra_email);

        var list_region = objJSON.REGION;
        var list_mitra_type = objJSON.MITRA_TYPE;
        
        $('.mitra_active_status').empty(); 
        $('.mitra_type').empty();
        $('.mitra_region').empty();

        if (objJSON.DATA.mitra_active_status != 0) {            
            $('.mitra_active_status').append("<option value='1'>Aktif</option>"); 
            $('.mitra_active_status').append("<option value='0'>Tidak Aktif</option>");
        }else 
        {
            $('.mitra_active_status').append("<option value='0'>Tidak Aktif</option>");
            $('.mitra_active_status').append("<option value='1'>Aktif</option>"); 
        } 

        $('.mitra_type').append("<option value='"+mitra_type_id+"'>"+mitra_type_name+"</option>");
           $.each(list_mitra_type, function(key, value) {
           if (value.mitra_type_id != mitra_type_id) {            
            $('.mitra_type').append("<option value='"+value.mitra_type_id+"'>"+value.mitra_type_name+"</option>"); 
           }                  
        });

        $('.mitra_region').append("<option value='"+mitra_region_id+"'>"+mitra_region_name+"</option>");
           $.each(list_region, function(key, value) {
           if (value.region_id != mitra_region_id) {            
            $('.department_id').append("<option value='"+value.region_id+"'>"+value.region_name+"</option>"); 
           }                  
        });

        
    }
});

   
}

function setContent(obj)
{
    var content = '';
    
           for (i = 0; i < obj.MESSAGE.length; i++) {
            if(obj.MESSAGE[i].mitra_active_status == '1') {
                var status = '<span class="badge badge-pill badge-success">Aktif</span>'; var status1 = 'Non-Aktif';}else {var status = '<span class="badge badge-pill badge-danger">Non-Aktif</span>';
            var status1 = 'Aktif'; }

                content += '<tr class="row-checked" id="tr_'+obj.MESSAGE[i].mitra_id+'">';
                content += ' <td><div class="text-block"><div class="text-block-container"><div class="text-block-text">'+obj.MESSAGE[i].mitra_name+'</div><div class="text-block-sub-text">'+obj.MESSAGE[i].mitra_email+'</div></div></div></td>';

                content += '<td><div class="text-block image"><div class="text-block-container"><div class="text-block-text">'+obj.MESSAGE[i].mitra_address+'</div><div class="text-block-sub-text">'+obj.MESSAGE[i].mitra_city+'</div></div></div></td>';

                content += '<td><div class="table-cell-block"><div class="text-block-container"><div class="text-block-text">'+obj.MESSAGE[i].mitra_phone+'</div></div></div></td>';
                content += '<td><div class="table-cell-block"><div class="text-block-container"><div class="text-block-text">'+obj.MESSAGE[i].region_name+'</div></div></div></td>';
                content += '<td><div class="table-cell-block"><div class="text-block-container"><div class="text-block-text">'+obj.MESSAGE[i].mitra_discount+' %</div></div></div></td>';
                content += '<td><div class="table-cell-block"><div class="text-block-container"><div class="text-block-text">'+toRp(obj.MESSAGE[i].mitra_batas_kredit)+'</div></div></div></td>';
                content += '<td><div class="table-cell-block"><div class="text-block-container"><div class="text-block-text">'+obj.MESSAGE[i].mitra_waktu_pelunasan+' Hari</div></div></div></td>';

                content += '<td><div class="table-cell-block status"><div class="status-block-container"><span class="user_active_status_'+obj.MESSAGE[i].mitra_id+'">'+status+'</span></div></div></td> ';
                
                content += '<td class="table-actions"><div class="dropdown"><a class="btn btn-link" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="fa fa-ellipsis-h"></span></a><div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1"><a class="dropdown-item" href="#" onClick=edit("<?php echo base_url();?>app/mitra/change_status/'+obj.MESSAGE[i].mitra_id+'")><span class="fa fa-pencil icon text-primary-on-hover"></span> Edit</a><a class="dropdown-item" href="#" onClick=changeStatus("<?php echo base_url();?>app/mitra/change_status/'+obj.MESSAGE[i].mitra_id+'")><span class="fa fa-remove icon text-danger-on-hover "></span><span class="aksi_user_active_status_'+obj.MESSAGE[i].mitra_id+'">'+status1+'</span></a></div></div> </td>';
                content += '</tr>';
                            
            }
            //console.log(content);
            $('#tbody').html(content);
            $('#pages_paging').html(obj.paging);
}
function toRp(a,b,c,d,e){e=function(f){return f.split('').reverse().join('')};b=e(parseInt(a,10).toString());for(c=0,d='';c<b.length;c++){d+=b[c];if((c+1)%3===0&&c!==(b.length-1)){d+='.';}}return'Rp.\t'+e(d)+',00'}
</script>
