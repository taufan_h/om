<div class="ks-column ks-page">
        <div class="ks-header">
            <section class="ks-title">
                <h3>Kategori</h3>

                <div class="ks-controls">
                    <button class="btn btn-primary-outline ks-light ks-filemanager-navigation-block-toggle" data-block-toggle=".ks-filemanager-page > .ks-navigation">Navigation</button>
                    <button class="btn btn-primary-outline ks-light ks-filemanager-info-block-toggle" data-block-toggle=".ks-filemanager-page > .ks-info">Info</button>
                </div>
            </section>
        </div>

        <div class="ks-content">
            <div class="ks-body ks-filemanager-page">
                <div class="ks-navigation ks-scrollable" data-auto-height>
    <div class="ks-wrapper">
        <div class="ks-separator">Tambah Baru</div>
            <ul class="ks-tree">
                <li class="ks-has-submenu">
                <a>
                    <form name="F1" id="F1">
                            <input type="hidden" name="category_id" class="form-control" id="category_id">
                            <input type="hidden" name="action" class="form-control" id="action" value="<?php echo $action; ?>">
                        <div class="form-group">
                            <label for="subject">Kategori Utama</label>
                            <select class="form-control ks-select category_parent_id" name="category_parent_id" id="select2-id-label-single category_parent_id">
                                <option value="0">(Tidak ada)</option>
                                <?php
                                    foreach (json_decode($list_category)->MESSAGE as $key) {
                                        # code...
                                        echo '<option value="'.$key->category_id.'">'.$key->category_name.'</option>';
                                    }
                                ?>
                               
                            </select>
                        </div>                                       
                        <div class="form-group">
                            <label for="subject">Nama Kategori</label>
                            <input type="text" class="form-control category_name" id="category_name" name="category_name" placeholder="Nama Kategori">
                        </div>                            
                       <div class="form-group">
                            <label for="subject">Status Aktif</label>
                            <select class="form-control ks-select category_active_status" id="select2-id-label-single category_active_status" name="category_active_status">
                                <option value="1">Aktif</option>
                                <option value="0">Tidak Aktif</option>
                            </select>
                        </div>                   
                        <div class="form-group">
                            <div class="ks-actions">
                                <button type="button" onclick="saveData()" class="btn btn-success">Kirim</button>
                                <input type="reset" class="btn btn-primary-outline ks-light ks-view-cancel" value="Batal"></input>
                                </button>
                            </div>
                        </div>
                    </form>
                </a>
            </li>
        </ul>
    </div>
</div>

<div class="ks-files">
    <div class="ks-header">
        <div class="ks-search input-icon icon-right icon icon-lg">
            <input id="input-group-icon-text" type="text" class="form-control cari" id="cari" placeholder="Cari...">
            <span class="icon-addon">
                <span class="fa fa-search" onclick="setData()"></span>
            </span>
        </div>
        <div class="ks-filters">
            <div class="ks-sortable btn-group">
                <button class="btn btn-primary-outline ks-light dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="btn-text-lighter">Urutkan</span>
                </button>
                <div class="dropdown-menu dropdown-menu-right ks-dropdown-menu-sortable urutkan">
                    <input name="sort_by" id="sort_by" type="hidden" value="category_name">
                    <a class="dropdown-item dropdown-item-checked" href="#" id="category_name" onclick="urutkan('category_name')">
                        <span class="ks-sort-by-text">Berdasarkan</span> Nama Kategori
                        <input name="radio" type="radio">
                    </a>
                    <a class="dropdown-item" href="#" id="category_active_status" onclick="urutkan('category_active_status')">
                        <span class="ks-sort-by-text">Berdasarkan</span> Status Aktif
                        <input name="radio" type="radio">
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="ks-content ks-scrollable" data-auto-height data-fix-height="40">
        <!--table-->
        <div class="card-block">
            <table class="table table-bordered vertical-align-middle tabelnya" id="tabelnya">
                <thead>
                <tr>
                    <th width="300">Nama Kategori</th>
                    <th width="30">Status Aktif</th>
                    <th>&nbsp;</th>
                </tr>
                </thead>
                <tbody id="tbody">
                <?php

                     foreach (json_decode($list_category)->MESSAGE as $key) {
                        # code...
                        switch ($key->category_active_status) {
                        case '1':
                            # code...
                        $status = '<span class="badge badge-pill badge-success">Aktif</span>';
                        $status1 = "Non-Aktif";
                        $status_balik = "0";
                            break;
                        case '0':
                            # code...
                        $status = '<span class="badge badge-pill badge-danger">Non-Aktif</span>';
                        $status1 = "Aktif";
                        $status_balik = "1";
                            break;
                    }
                        echo '
                        <tr id="tr_'.$key->category_id.'">
                    <td>
                        <div class="text-block">
                            <div class="text-block-container">
                                <div class="text-block-text">'.$key->category_name.'</div>
                            </div>
                        </div>
                    </td>
                    <td>
                       <div class="table-cell-block status">
                                <div class="status-block-container">
                                <span class="user_active_status_'.$key->category_id.'">'.$status.'</span>
                                </div>
                            </div>
                    </td>        
                    <td class="table-actions">
                        <div class="dropdown">
                            <a class="btn btn-link" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="fa fa-ellipsis-h"></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                                <a class="dropdown-item" href="#" onClick=edit("'.base_url().'app/kategori/edit/'.$key->category_id.'")>
                                    <span class="fa fa-pencil icon text-primary-on-hover"></span> Edit
                                </a>
                                <a class="dropdown-item" href="#" onClick=changeStatus("'.base_url().'app/kategori/change_status/'.$key->category_id.'")><span class="fa fa-remove icon text-danger-on-hover "></span><span class="aksi_user_active_status_'.$key->category_id.'">'.$status1.'</span></a>
                            </div>
                        </div>
                    </td>
                </tr>
                        ';
                    }
                ?>
               
                </tbody>
            </table>

         
            <!--paging-->
            <div class="ks-items-block pages_paging" id="pages_paging"><nav>
                <?php echo $pagination; ?></nav>
            </div>
            <!--end paging-->
        </div>
        <!--end--> 
    </div>
</div>
<script type="text/javascript">



function changeStatus(url)
{
    $.ajax({
    type: "GET",
    url: url,
    cache: false,                  
    success:function(response) {
        console.log(response);
        var objJSON = JSON.parse(response);
        var id = objJSON.ID;
        var status_name = objJSON.STATUS_NAME;
        if (objJSON.STATUS_CODE == '1') {
            var revert_status_name = 'Non-Aktif';
        }else
        {
            var revert_status_name = 'Aktif';
        }
        $('.aksi_user_active_status_'+id).html(revert_status_name);
        if (objJSON.STATUS_CODE == '1') {
            $('.user_active_status_'+id).html('<span class="badge badge-pill badge-success">Aktif</span>');
        }else 
        {
            $('.user_active_status_'+id).html('<span class="badge badge-pill badge-danger">Non-Aktif</span>');
        }
    }
});
}
// fungsi tambah baru
function saveData()
{
    var url = $('#action').val();
        $.ajax({
    type: "post",
    url: url,
    cache: false,               
    data: $('#F1').serialize(),    
    success:function(response) {
      //console.log(response); 
      var objJSON = JSON.parse(response); 
      if (objJSON.RC != 999) {
            alert(objJSON.MESSAGE);
      }else 
      {
        //console.log(objJSON.RC);
            alert(objJSON.MESSAGE);
            console.log(objJSON.NEW_DATA);
            var new_data = objJSON.NEW_DATA;
            var category_parent_id
        $('.category_parent_id').empty(); 
        $('.category_parent_id').append("<option value='0'>(Tidak Ada)</option>");
           $.each(new_data, function(key, value) {                   
            $('.category_parent_id').append("<option value='"+value.category_id+"'>"+value.category_name+"</option>"); 
                            
        });

       setData();
        bersihkanForm();
       // setPaging();
      }    
    
    }
}); 
}
function bersihkanForm()
{
    $('#F1').trigger('reset')
}

// untuk menampilkan data ke tabel
function setData()
{
    var cari = $('.cari').val();
    var sort_by = $('#sort_by').val();
     $.ajax({
        type: "post",
        url: '<?php echo base_url()."app/kategori/search";?>',
        data: {sort_by : sort_by, keyword : cari},
        success: function(res){
            console.log(res);
            var obj = JSON.parse(res);
        if (obj.RC == 999) {
           setContent(obj);
        }
        
        }
         
    });
 

}


function urutkan(sort_by)
{
    $('#sort_by').val(sort_by);
    $('.urutkan a').removeClass('dropdown-item-checked');
    var sort =  $('#sort_by').val();
    setData();
}
function edit(url)
{
  $('.tambah_baru').html('Edit Data');
  $.ajax({
    type: "GET",
    url: url,
    cache: false,                  
    success:function(response) {
        console.log(response);
        var objJSON = JSON.parse(response);

        var category_id = objJSON.DATA.category_id;
        var category_parent_id = objJSON.DATA.category_parent_id;
        var action = objJSON.ACTION;
        var category_name = objJSON.DATA.category_name;
        var category_active_status = objJSON.DATA.category_active_status;

       var old_parent_id = objJSON.DATA.category_parent_id;
       var listnya = objJSON.CATEGORY;

        $('#category_name').val(category_name);
        $('#category_id').val(category_id);               
        $('#category_active_status').val(category_active_status);
        $('#action').val(objJSON.ACTION);
       
        $('.category_active_status').empty(); 
        $('.category_parent_id').empty(); 
       

            if (objJSON.DATA.category_active_status != 0) {            
            $('.category_active_status').append("<option value='1'>Aktif</option>"); 
            $('.category_active_status').append("<option value='0'>Tidak Aktif</option>");
        }else 
        {
            $('.category_active_status').append("<option value='0'>Tidak Aktif</option>");
            $('.category_active_status').append("<option value='1'>Aktif</option>"); 
        }

        if (old_parent_id == 0) {            
            $('.category_parent_id').append("<option value='0'>(Tidak Ada)</option>");             
           }else
           {
             $.each(listnya, function(key, value) {
                if (value.category_id == category_parent_id) {            
                    $('.category_parent_id').append("<option value='"+value.category_id+"'>"+value.category_name+"</option>"); 
                }                  
            });
           }
        
           $.each(listnya, function(key, value) {
           if (value.category_id != category_id) {            
            $('.category_parent_id').append("<option value='"+value.category_id+"'>"+value.category_name+"</option>"); 
           
           }                  
        });
    }
});

   
}

function setContent(obj)
{
    var content = '';
    
           for (i = 0; i < obj.MESSAGE.length; i++) {
            if(obj.MESSAGE[i].category_active_status == '1') {
                var status = '<span class="badge badge-pill badge-success">Aktif</span>'; var status1 = 'Non-Aktif';}else {var status = '<span class="badge badge-pill badge-danger">Non-Aktif</span>';
            var status1 = 'Aktif'; }

                content += '<tr id="tr_'+obj.MESSAGE[i].category_id+'">';
                content += ' <td><div class="text-block"><div class="text-block-container"><div class="text-block-text">'+obj.MESSAGE[i].category_name+'</div></div></div></td>';
               
                content += '<td><div class="table-cell-block status"><div class="status-block-container"><span class="user_active_status_'+obj.MESSAGE[i].category_id+'">'+status+'</span></div></div></td> ';
                
                content += '<td class="table-actions"><div class="dropdown"><a class="btn btn-link" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="fa fa-ellipsis-h"></span></a><div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1"><a class="dropdown-item" href="#" onClick=edit("<?php echo base_url();?>app/kategori/edit/'+obj.MESSAGE[i].category_id+'")><span class="fa fa-pencil icon text-primary-on-hover"></span> Edit</a><a class="dropdown-item" href="#" onClick=changeStatus("<?php echo base_url();?>app/kategori/change_status/'+obj.MESSAGE[i].category_id+'")><span class="fa fa-remove icon text-danger-on-hover "></span><span class="aksi_user_active_status_'+obj.MESSAGE[i].category_id+'">'+status1+'</span></a></div></div> </td>';
                content += '</tr>';
                            
            }
            //console.log(content);
            $('#tbody').html(content);
            $('#pages_paging').html(obj.paging);
}

</script>

          