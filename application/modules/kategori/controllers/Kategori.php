<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori extends CI_Controller {

public $client_id;
    function __construct() {
       parent::__construct();
       $this->cek_session();
       $this->load->model('m_kategori','model');
       $this->client_id   = $this->session->userdata('client_id');  
    }


 function cek_session()
    {       
        if($this->session->userdata("status_login") != true)
        {
           $this->session->set_flashdata('msg_login', 'Anda harus login terlebih dahulu.');
            redirect(base_url());
        }
    }

    function kategori_view($offset = null)
    {
        $sort_by = $this->input->get('sort_by');
        if(!$sort_by || $sort_by == '-') {$sort_by = 'category_name';}
        if($offset == null) {$offset = 0;}
        $limit = 20;     
        
        $data['list']      = $this->model->select_dt($sort_by, $offset, $limit)->result();
        $total_data = $this->model->total_dt()->num_rows();   
       
        $this->pagination->initialize($this->config_paging($limit, $total_data,"load"));
       
        $data['action'] = base_url().'app/kategori/add/proccess';
        $data['pagination'] = $this->pagination->create_links();

     
      $arr = array();
      $config['ul_class']='treeview';
      $config['client_id']=$this->client_id;
      $config['table']='tbl_category';
      $this->load->library('tree',$config);

    foreach ($this->model->select_where('tbl_category','client_id',$this->client_id)->result() as $key) {
        if($key->category_parent_id == 0)
        {
          $aa['category_id'] = $key->category_id;
          $aa['category_name'] = $key->category_name;
          $aa['category_active_status'] = $key->category_active_status;
          array_push($arr,$aa);
        }else
        {
         // $s = $this->model->select_where('tbl_category','category_parent_id',$key->category_id)->row();
          $aa['category_id'] = $key->category_id;
          $aa['category_name'] = $this->tree->getTree($key->category_id);
          $aa['category_active_status'] = $key->category_active_status;
          array_push($arr,$aa);
        }
      }
    $json_e = array('RC' =>'200', 'MESSAGE' => $arr );    
    $data['list_category'] = json_encode($json_e);
  

      $this->template->front('kategori/view',$data);
        
    }


  
  
     function search($offset = null)
    {
      $sort_by = $this->input->post('sort_by');
      $keyword = $this->input->post('keyword');

       // $sort_by = 'mitra_active_status';
       // $keyword = '';

        if(!$sort_by || $sort_by == '-') 
          {
            $sort_by = 'category_name';

          }
          if ($sort_by == 'category_active_status') {
            # code...
             $by = 'desc';
          }else
          {
             $by = 'asc';
          }
        if($offset == null) {$offset = 0;}
        $limit = 20;
       
       $arr = array();
      $config['ul_class']='treeview';
      $config['client_id']=$this->client_id;
      $config['table']='tbl_category';
      $this->load->library('tree',$config);

    foreach ($this->model->select_search($sort_by,$by, $offset, $limit, $keyword)->result() as $key) {
        if($key->category_parent_id == 0)
        {
          $aa['category_id'] = $key->category_id;
          $aa['category_name'] = $key->category_name;
          $aa['category_active_status'] = $key->category_active_status;
          array_push($arr,$aa);
        }else
        {
         // $s = $this->model->select_where('tbl_category','category_parent_id',$key->category_id)->row();
          $aa['category_id'] = $key->category_id;
          $aa['category_name'] = $this->tree->getTree($key->category_id);
          $aa['category_active_status'] = $key->category_active_status;
          array_push($arr,$aa);
        }
      }

        
        $data['list']      = $this->model->select_search($sort_by,$by, $offset, $limit, $keyword)->result();
        $total_data = $this->model->total_search($keyword)->num_rows();   
       
      $this->pagination->initialize($this->config_paging($limit, $total_data,"search"));
        $data['sort_by']       = $sort_by;
        $data['search']     = $keyword;
        $data['pagination'] = $this->pagination->create_links();
       $arr = array('RC' => 999, 'MESSAGE' =>  $arr, 'search' => $data['search'], 'total' => $total_data, 'paging' => $data['pagination']);
       $json = json_encode($arr);
       echo $json; 
     
        
            //$this->load->view('user/admin_view_user',$data);
        
    }
    
            
    function config_paging($limit, $total_data,$type)
    {
     
        $config['base_url']     = site_url().'app/kategori/search/';
      
        $config['total_rows']   = $total_data;
        $config['per_page']     = $limit;
        $config["uri_segment"]  = 4;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"]    = floor($choice);
        //$config['page_query_string'] = TRUE;
        //config for bootstrap pagination class integration
        $config['attributes'] = array('class' => 'page-link');
        $config['full_tag_open']    = '<ul class="pagination">';
        $config['full_tag_close']   = '</ul>';
        $config['first_link']       = false;
        $config['last_link']        = false;
        $config['first_tag_open']   = '<li class="page-item">';
        $config['first_tag_close']  = '</li>';
        $config['prev_link']        = '&laquo';
        $config['prev_tag_open']    = '<li class="prev">';
        $config['prev_tag_close']   = '</li>';
        $config['next_link']        = '&raquo';
        $config['next_tag_open']    = '<li class="page-item">';
        $config['next_tag_close']   = '</li>';
        $config['last_tag_open']    = '<li class="page-item">';
        $config['last_tag_close']   = '</li>';
        $config['cur_tag_open']     = '<li class="page-item active"><a class="page-link" href="#">';
        $config['cur_tag_close']    = '</a></li>';
        $config['num_tag_open']     = '<li>';
        $config['num_tag_close']    = '</li>';
        
        return $config;
    }
    
    function add_proccess()
    {
        
        $category_name           = $this->input->post('category_name');
        $category_parent_id           = $this->input->post('category_parent_id');
        $category_active_status              = $this->input->post('category_active_status');

        $data = array(
            'client_id'             => $this->client_id,
            'category_name'         => $category_name,
            'category_parent_id'         => $category_parent_id,
            'category_active_status'              => $category_active_status
        );
       
        if($category_name == '')
        {
        $json_e  = array('RC' =>'998','MESSAGE' => 'Nama Kategori Harus Diisi', 'URL' => base_url().'app/kategori');
        }else{
            $insert = $this->model->insert_data('tbl_category',$data);  

      $arr = array();
         $config['ul_class']='treeview';
      $config['client_id']=$this->client_id;
      $config['table']='tbl_category';
      $this->load->library('tree',$config);

    foreach ($this->model->select_where('tbl_category','client_id',$this->client_id)->result() as $key) {
        if($key->category_parent_id == 0)
        {
          $aa['category_id'] = $key->category_id;
          $aa['category_name'] = $key->category_name;
          $aa['category_active_status'] = $key->category_active_status;
          array_push($arr,$aa);
        }else
        {
         // $s = $this->model->select_where('tbl_category','category_parent_id',$key->category_id)->row();
          $aa['category_id'] = $key->category_id;
          $aa['category_name'] = $this->tree->getTree($key->category_id);
          $aa['category_active_status'] = $key->category_active_status;
          array_push($arr,$aa);
        }
      }
    $new_data = json_encode(array('RC' =>'200', 'MESSAGE' => $arr ));    
   

        $json_e  = array('RC' =>'999','MESSAGE' => 'Sukses Tambah Data', 'NEW_DATA' => $arr);
        }
        echo json_encode($json_e);
       
    }
    
    function edit($id)
    {      
        $get                    = $this->model->ambil_data('tbl_category','category_id',$id)->row();
        $config['ul_class']='treeview';
        $config['client_id']=$this->client_id;
        $config['table']='tbl_category';
        $this->load->library('tree',$config);

        $arr = array();
        foreach ($this->model->select_where('tbl_category','client_id',$this->client_id)->result() as $key) {
            if($key->category_parent_id == 0)
            {
              $aa['category_id'] = $key->category_id;
              $aa['category_name'] = $key->category_name;
              $aa['category_active_status'] = $key->category_active_status;
              array_push($arr,$aa);
            }else
            {
             // $s = $this->model->select_where('tbl_category','category_parent_id',$key->category_id)->row();
              $aa['category_id'] = $key->category_id;
              $aa['category_name'] = $this->tree->getTree($key->category_id);
              $aa['category_active_status'] = $key->category_active_status;
              array_push($arr,$aa);
            }
          }
      $json_e = array('RC' =>'200', 'MESSAGE' => $arr );    
     // $data['list_category'] = json_encode($json_e);
      $data['action']    = 'kategori/edit/proccess';

        //$this->load->view('user/formodel',$data);
        echo json_encode(array('RC' => '999', 'DATA' => $get,'ACTION' => $data['action'],'CATEGORY' =>$arr ));
    }
    function edit_proccess()
    {
         
       
        $category_id              = $this->input->post('category_id');       
        $category_name           = $this->input->post('category_name');
        $category_parent_id           = $this->input->post('category_parent_id');
        $category_active_status              = $this->input->post('category_active_status');

        $data = array(
            'client_id'             => $this->client_id,
            'category_name'         => $category_name,
            'category_parent_id'         => $category_parent_id,
            'category_active_status'              => $category_active_status
        );
        
        if($category_name == '')
        {
        $json_e  = array('RC' =>'998','MESSAGE' => 'Nama Kategori Harus Diisi', 'URL' => base_url().'app/kategori');
        }else{
            $this->model->update_data('tbl_category','category_id',$category_id,$data);
        $json_e  = array('RC' =>'999','MESSAGE' => 'Data Berhasil diedit', 'URL' => base_url().'app/kategori');
            
        }
        echo json_encode($json_e);
    }
    function change_status($id)
    {
        
        $status_data = $this->model->select_where('tbl_category','category_id',$id)->row();
        $status = $status_data->category_active_status;

        switch ($status) {
          case '1':
          $status_code = '0';
          $status_name = 'Non-Aktif';
            break;
           case '0':
          $status_code = '1';
          $status_name = 'Aktif';
            break;
        }

        $data = array(
           'category_active_status'     => $status_code
        );
        $this->model->update_data('tbl_category','category_id',$id,$data);
        $this->session->set_flashdata('data','Sukses ubah status.');
        
        $json_e  = array('RC' =>'999','ID' => $id,'STATUS_CODE' => $status_code,'STATUS_NAME' => $status_name, 'MESSAGE' => 'Sukses');
        echo json_encode($json_e);
    }
    

    

}
