<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>ORDERMATIX</title>

    <meta http-equiv="X-UA-Compatible" content=="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" type="text/css" href="<?php echo libs_url; ?>bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo libs_url; ?>font-awesome/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo libs_url; ?>tether/css/tether.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo assets_url; ?>styles/common.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo assets_url; ?>styles/pages/auth.min.css">
</head>
<body>

<div class="ks-page">
    <div class="ks-body">
        <div class="ks-logo">ORDERMATIX</div>

        <div class="card panel panel-default ks-light ks-panel">
            <div class="card-block">

                   <?php echo form_open(base_url()."login/proses",'id="myform", name="form_login"');?>
                   <?php if ($this->session->flashdata('msg_login')) {
                # code...
                    echo '<center><h4 class="ks-header">'.$this->session->flashdata('msg_login').'</h4></center>';
                
            }else 
                echo '
                    <h4 class="ks-header">Selamat datang!</h4>';
            ?>
                    <div class="form-group">
                        <div class="input-icon icon-left icon-lg icon-color-primary">
                            <input type="text" name="username" class="form-control" required placeholder="Nama User">
                            <span class="icon-addon">
                                <span class="fa fa-at"></span>
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-icon icon-left icon-lg icon-color-primary">
                            <input type="password" name="password" class="form-control" required placeholder="Kata Sandi">
                            <span class="icon-addon">
                                <span class="fa fa-key"></span>
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-block">Masuk</button>
                    </div>
                    <div class="ks-social">
                        <div>&copy; <?php echo date("Y") ?> ORDERMATIX</div>
                    </div>
                    <?php form_close();?>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo libs_url; ?>jquery/jquery.min.js"></script>
<script src="<?php echo libs_url; ?>tether/js/tether.min.js"></script>
<script src="<?php echo libs_url; ?>bootstrap/js/bootstrap.min.js"></script>
</body>
</html>