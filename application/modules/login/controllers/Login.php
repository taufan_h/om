<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	 function __construct() {
        parent::__construct();
        $this->load->model('M_login','model');
    }


    public function index()
	{            
            $this->login();
	}

    function login()
    {
        if($this->session->userdata("status_login") == true)
        {
            header('location:'.base_url().'app/dashboard');
        }else
        {
            $data['title'] = "Masuk - Ordermatix";
            $this->load->view("login/dashboard",$data); 
        }
    }
    function login_proccess()
    {
       	$username      = $this->input->post('username');
        $password   =  $this->input->post('password');
        
         $replacements = array('1' => '2',
                      '2' => '3', 
                      '3' => '4',
                      '4' => '5',
                      '5' => '6',
                      'a' => 'b',
                      'b' => 'c',
                      'c' => 'd',
                     );
        $capcay             = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxysABCDEFGHIJKLMNOPQRSTUVWXYZ", 5)), 0, 5);
        $acak               = substr($capcay,0,6);
        $txt_password2      = $acak.md5($password);
        $txt_new_password   = strtr($txt_password2,$replacements);
        $txt_new_password = substr($txt_new_password, 5);

        $data = array(
            'username' => $username,
            'password' => $password);
       $hasil = $this->model->cek_login($data);

       if ($hasil['valid_username'] != '1') {
           # code...
            $this->session->set_flashdata('msg_login', 'User name tidak ditemukan');
            redirect(base_url());
       }
       if ($hasil['valid_status'] != '1') {
           # code...
            $this->session->set_flashdata('msg_login', 'User name tidak aktif');
            redirect(base_url());
       }
       if (substr($hasil['valid_password']->user_password,5) != $txt_new_password) {
           # code...
            $this->session->set_flashdata('msg_login', 'password salah');
            redirect(base_url());
       }
       if ($hasil['valid_username'] == '1' && $hasil['valid_status'] == '1' && substr($hasil['valid_password']->user_password,5) == $txt_new_password) {
           # code...
        $data = $this->model->ambil_data_login(array('username' => $username))->row();

           $data_session = array(                
                'user_id'           => $data->user_id,
                'user_fullname'     => $data->user_fullname,
                'username'          => $data->username,
                'department_id'     => $data->departemen_id,
                'client_id'         => $data->client_id,
                'parent_user_id'    => $data->parent_user_id,
                'status_login'      => true,
                'user_photo'        => $data->user_photo,
                'type_id'           => $data->type_id
            );
           $data_update = array(
            'user_last_login' => date('Y-m-d H:i:s'));
           $this->model->update_data('tbl_user',$data_update,$data->user_id);
            $this->session->set_userdata($data_session);
            $this->session->sess_expire_on_close = FALSE;
            redirect(base_url()."app/dashboard"); 
       }

       
    }
            
    function logout()
    {
        $this->session->sess_destroy(); 
        redirect(base_url());
    }
    
   
}
