

    <div class="ks-column ks-page">
        <div class="ks-header">
            <section class="ks-title">
                <h3>Hasil Pantauan</h3>

                <div class="ks-controls">
                    <button class="btn btn-primary-outline ks-light ks-filemanager-navigation-block-toggle" data-block-toggle=".ks-filemanager-page > .ks-navigation">Navigation</button>
                    <button class="btn btn-primary-outline ks-light ks-filemanager-info-block-toggle" data-block-toggle=".ks-filemanager-page > .ks-info">Info</button>
                </div>
            </section>
        </div>

        <div class="ks-content">
            <div class="ks-body ks-filemanager-page">
<div class="ks-navigation ks-scrollable" data-auto-height>
    <div class="ks-wrapper">
            <ul class="ks-tree">
                <li class="ks-has-submenu">
                <a>
                  
                        <div class="form-group">
                            <label for="subject">Pilih User</label>
                            <select class="form-control ks-select pilih_user" id="select2-id-label-single pilih_user">
                            <option value='-'>Semua User</option>
                                <?php
                                    foreach ($user as $key) {
                                        # code...
                                        echo "<option value='".$key->user_id."'>".$key->user_fullname."</option>";
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="subject">Pilih Periode</label>
                            <input type="text" name="daterange" class="form-control ks-daterange col-xl-12" id="periode">
                        </div>                  
                        <div class="form-group">
                            <div class="ks-actions">
                                <button type="button" class="btn btn-success btn-bg btn-block" onclick="setData_period()">Lihat</button>
                            </div>
                        </div>
                   
                </a>
            </li>
        </ul>
    </div>
</div>

<div class="ks-files">
    <div class="ks-header">
        <div class="ks-search input-icon icon-right icon icon-lg">
            <input id="input-group-icon-text" type="text" class="form-control cari" placeholder="Cari...">
            <span class="icon-addon">
                <span class="fa fa-search" onclick="setData()"></span>
            </span>
        </div>
        <div class="ks-filters">
            <div class="ks-sortable btn-group">
                <button class="btn btn-primary-outline ks-light dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="btn-text-lighter">Urutkan</span>
                </button>
                <div class="dropdown-menu dropdown-menu-right ks-dropdown-menu-sortable urutkan">
                        <input name="sort_by" id="sort_by" type="hidden" value="user_fullname">
                    <a class="dropdown-item dropdown-item-checked" href="#" id="user_fullname" onclick="urutkan('user_fullname')">
                        <span class="ks-sort-by-text">Berdasarkan</span> Nama User
                        <input name="radio" type="radio">
                    </a>
                     <a class="dropdown-item" href="#" id="mitr_name" onclick="urutkan('mitr_name')">
                        <span class="ks-sort-by-text">Berdasarkan</span> Nama Mitra
                        <input name="radio" type="radio">
                    </a>
                     <a class="dropdown-item" href="#" id="activity_result_battery_level" onclick="urutkan('activity_result_battery_level')">
                        <span class="ks-sort-by-text">Berdasarkan</span> Baterai
                        <input name="radio" type="radio">
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="ks-content ks-scrollable" data-auto-height data-fix-height="40">
        <!--table-->
        <div class="card-block">
            <table class="table table-bordered vertical-align-middle tabelnya" id="tabelnya">
                <thead>
                <tr>
                    <th width="110">Nama Pengguna</th>
                    <th width="10%">Nama Mitra</th>
                    <th width="160">Lokasi Tujuan</th>
                    <th width="20">Lokasi Terdeteksi</th>
                    <th width="80" style="text-align: center">Baterai</th>
                    <th width="20">Catatan</th>
                    <th>&nbsp;</th>
                </tr>
                </thead>
                <tbody id="tbody">
                <?php
                    foreach (json_decode($list) as $key) {
                        if ($key->activity_result_battery_level < 25) {
                            # code...
                            $battery = '<span class="badge badge-pill badge-danger">'.$key->activity_result_battery_level.' %</span>';
                        }else
                        if ($key->activity_result_battery_level >= 25 && $key->activity_result_battery_level < 50) 
                        {
                            $battery = '<span class="badge badge-pill badge-warning">'.$key->activity_result_battery_level.' %</span>';
                        }else
                        if ($key->activity_result_battery_level >= 50 ) 
                        {
                            $battery = '<span class="badge badge-pill badge-success">'.$key->activity_result_battery_level.' %</span>';
                        }
                        # code...
                        echo '
                        <tr>
                    <td>
                        <div class="text-block">
                            <div class="text-block-container">
                                <div class="text-block-text">'.$key->user_fullname.'</div>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="text-block">
                            <div class="text-block-container">
                                <div class="text-block-text">'.$key->mitra_name.'</div>
                                <div class="text-block-sub-text">'.$key->mitra_email.'</div>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="table-cell-block image">
                            <div class="text-block-container">
                                <div class="text-block-text">'.$key->mitra_address.'</div>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="table-cell-block image">
                            <div class="text-block-container">
                                <div class="text-block-text">'.$key->lokasi_terdeteksi.'</div>
                            </div>
                        </div>
                    </td>                    
                    <td style="text-align: center">
                        <div class="table-cell-block">
                            <div class="text-block-container">'.$battery.'
                            </div>
                        </div>
                    </td>                                      
                    <td>
                        <div class="table-cell-block">
                            <div class="text-block-container">
                                <div class="text-block-text">'.$key->activity_result_notes.'</div>
                            </div>
                        </div>
                    </td>        
                    <td class="table-actions">
                        <div class="dropdown">
                            <a class="btn btn-link" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="fa fa-ellipsis-h"></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                                <a class="dropdown-item" href="'.base_url().'app/pantauan/detail/'.$key->activity_result_id.'">
                                    <span class="fa fa-eye icon text-primary-on-hover"></span> Lihat
                                </a>
                            </div>
                        </div>
                    </td>
                </tr>';
                    }
                ?>               
                </tbody>
            </table>

            <!--paging-->
            <div class="ks-items-block pages_paging_pantauan" id="pages_paging"><nav>
                <?php echo $pagination; ?></nav>
            </div>
            <!--end paging-->
        </div>
        <!--end--> 
    </div>
</div>
<script type="text/javascript">
    
// untuk menampilkan data ke tabel
function setData()
{
    var cari = $('.cari').val();
    var sort_by = $('#sort_by').val();
    var periode = $('#periode').val();
    var pilih_user = $('.pilih_user option:selected').val();
    $.ajax({
        type: "post",
        url: '<?php echo base_url()."app/pantauan/search";?>',
        data: {sort_by : sort_by, keyword : cari, periode : periode, pilih_user:pilih_user},
        success: function(res){
            console.log(res);
            var obj = JSON.parse(res);
        if (obj.RC == 999) {
           setContent(obj);
        }
        
        }
         
    });
}
function setData_period()
{
    $('.cari').val('');
    var cari = $('.cari').val();
    var sort_by = $('#sort_by').val();
    var periode = $('#periode').val();
    var pilih_user = $('.pilih_user option:selected').val();
    $.ajax({
        type: "post",
        url: '<?php echo base_url()."app/pantauan/search";?>',
        data: {sort_by : sort_by, keyword : cari, periode : periode, pilih_user:pilih_user},
        success: function(res){
            console.log(res);
            var obj = JSON.parse(res);
        if (obj.RC == 999) {
           setContent(obj);
        }
        
        }
         
    });
}

function urutkan(sort_by)
{
    $('#sort_by').val(sort_by);    
    $('.urutkan a').removeClass('dropdown-item-checked');
    var sort =  $('#sort_by').val();
    setData();
}

function setContent(obj)
{
    var content = '';
    var battery = '';
           for (i = 0; i < obj.MESSAGE.length; i++) {
            if(obj.MESSAGE[i].activity_result_battery_level  < 25) {
                          
                            battery = '<span class="badge badge-pill badge-danger">'+obj.MESSAGE[i].activity_result_battery_level+' %</span>';
                        }else
                        if (obj.MESSAGE[i].activity_result_battery_level >= 25 && obj.MESSAGE[i].activity_result_battery_level < 50) 
                        {
                            battery = '<span class="badge badge-pill badge-warning">'+obj.MESSAGE[i].activity_result_battery_level+' %</span>';
                        }else
                        if (obj.MESSAGE[i].activity_result_battery_level >= 50 ) 
                        {
                            battery = '<span class="badge badge-pill badge-success">'+obj.MESSAGE[i].activity_result_battery_level+' %</span>';
                        }

            content += '<tr>';
            content += '<td><div class="text-block"><div class="text-block-container"><div class="text-block-text">'+obj.MESSAGE[i].user_fullname+'</div></div></div></td>';
            content += '<td><div class="text-block"><div class="text-block-container"><div class="text-block-text">'+obj.MESSAGE[i].mitra_name+'</div><div class="text-block-sub-text">'+obj.MESSAGE[i].mitra_email+'</div></div></div></td>';
            content += '<td><div class="table-cell-block image"><div class="text-block-container"><div class="text-block-text">'+obj.MESSAGE[i].mitra_address+'</div></div></div></td>';
            content += '<td><div class="table-cell-block image"><div class="text-block-container"><div class="text-block-text">'+obj.MESSAGE[i].lokasi_terdeteksi+'</div></div></div></td> ';
            content += '<td style="text-align: center"><div class="table-cell-block"><div class="text-block-container">'+battery+'</div></div></td>';
            content += '<td><div class="table-cell-block"><div class="text-block-container"><div class="text-block-text">'+obj.MESSAGE[i].activity_result_notes+'</div></div></div></td> ';
            content += '<td class="table-actions"><div class="dropdown"><a class="btn btn-link" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="fa fa-ellipsis-h"></span></a><div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1"><a class="dropdown-item" href="<?php echo base_url();?>app/pantauan/detail/'+obj.MESSAGE[i].activity_result_id+'"><span class="fa fa-eye icon text-primary-on-hover"></span> Lihat</a></div></div></td>';
            content += '</tr>';

            }
            //console.log(content);
            $('#tbody').html(content);
            $('#pages_paging').html(obj.paging);
}
</script>
