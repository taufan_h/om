<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pantauan extends CI_Controller {

public $client_id;
public $user_id;
    function __construct() {
       parent::__construct();
       $this->cek_session();
       $this->load->model('m_pantauan','model');
       $this->client_id   = $this->session->userdata('client_id');  
       $this->user_id   = $this->session->userdata('user_id'); 
    }


 function cek_session()
    {       
        if($this->session->userdata("status_login") != true)
        {
           $this->session->set_flashdata('msg_login', 'Anda harus login terlebih dahulu.');
            redirect(base_url());
        }
    }

    function pantauan_view($offset = null)
    {
      $hari_ini = "2017-03-01";
        $sort_by = $this->input->get('sort_by');
        if(!$sort_by || $sort_by == '-') {$sort_by = 'user_fullname';}
        if($offset == null) {$offset = 0;}
        $limit = 20;     
        
        $list = array();

        $lst = $this->model->select_dt($sort_by, $offset, $limit,$hari_ini)->result();

          foreach ($lst as $key) {

            # code...

            $acakResultId = $this->acakId($key->activity_result_id);
            
            $param['user_id']         = $key->user_id;
            $param['mitra_name']      = $key->mitra_name;
            $param['activity_result_battery_level']         = $key->activity_result_battery_level;
            $param['user_fullname']         = $key->user_fullname;
            $param['mitra_email']         = $key->mitra_email;
            $param['mitra_address']         = $key->mitra_address;
            $param['activity_result_notes']         = $key->activity_result_notes;
            $param['activity_result_id']         = $acakResultId;
            $param['lokasi_terdeteksi']         = $this->getaddress($key->activity_result_lat_in,$key->activity_result_lng_in);

            array_push($list, $param);
          }

        $data['list']      =json_encode( $list);
        $total_data = $this->model->total_dt($hari_ini)->num_rows();   
       
       $where = array(
          'type_id' => '2',
          'client_id' => $this->client_id
        );
        $data['user'] = $this->model->select_where('tbl_user',$where)->result();
        $this->pagination->initialize($this->config_paging($limit, $total_data,"load"));
       
        $data['pagination'] = $this->pagination->create_links();
      
     $this->template->front('pantauan/view',$data);
     //   print_r($data['list']);
        
    }
  
     function search($offset = null)
    {
      $sort_by = $this->input->post('sort_by');
      $keyword = $this->input->post('keyword');
      $periode = $this->input->post('periode');
      $pilih_user = $this->input->post('pilih_user');

    /*  $exp = explode('-', $periode);
      $start_date = date('Y-m-d', strtotime(trim($exp[0])));
      $end_date   = date('Y-m-d', strtotime(trim($exp[1])));*/

      $start_date = "2017-03-01";
      $end_date = "2017-03-01";

       // $sort_by = 'mitra_active_status';
       // $keyword = '';

        if(!$sort_by || $sort_by == '-') 
          {
            $sort_by = 'user_fullname';

          }
         $by = 'asc';

        if($offset == null) {$offset = 0;}
        $limit = 20;
       
       
         $list = array();

        $lst = $this->model->select_search($sort_by,$by, $offset, $limit, $keyword, $start_date,$end_date,$pilih_user)->result();

          foreach ($lst as $key) {
            $acakResultId = $this->acakId($key->activity_result_id);
            # code...
            $param['user_id']         = $key->user_id;
            $param['mitra_name']      = $key->mitra_name;
            $param['activity_result_battery_level']         = $key->activity_result_battery_level;
            $param['user_fullname']         = $key->user_fullname;
            $param['mitra_email']         = $key->mitra_email;
            $param['mitra_address']         = $key->mitra_address;
            $param['activity_result_notes']         = $key->activity_result_notes;
            $param['activity_result_id']         = $acakResultId;
            $param['lokasi_terdeteksi']         = $this->getaddress($key->activity_result_lat_in,$key->activity_result_lng_in);

            array_push($list, $param);
          }


        $total_data = $this->model->total_search($keyword,$start_date,$end_date,$pilih_user)->num_rows();   
       

      $this->pagination->initialize($this->config_paging($limit, $total_data,"search"));
        $data['sort_by']       = $sort_by;
        $data['search']     = $keyword;
        $data['pagination'] = $this->pagination->create_links();

        
       $arr = array('RC' => 999, 'MESSAGE' =>  $list, 'search' => $data['search'],'pilih_user' => $pilih_user, 'total' => $total_data, 'paging' => $data['pagination']);
       $json = json_encode($arr);
       echo $json; 
     
        
            //$this->load->view('user/admin_view_user',$data);
        
    }
    
            
    function config_paging($limit, $total_data,$type)
    {
     
        $config['base_url']     = site_url().'app/pantauan/search/';
      
        $config['total_rows']   = $total_data;
        $config['per_page']     = $limit;
        $config["uri_segment"]  = 4;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"]    = floor($choice);
        //$config['page_query_string'] = TRUE;
        //config for bootstrap pagination class integration
        $config['attributes'] = array('class' => 'page-link');
        $config['full_tag_open']    = '<ul class="pagination">';
        $config['full_tag_close']   = '</ul>';
        $config['first_link']       = false;
        $config['last_link']        = false;
        $config['first_tag_open']   = '<li class="page-item">';
        $config['first_tag_close']  = '</li>';
        $config['prev_link']        = '&laquo';
        $config['prev_tag_open']    = '<li class="prev">';
        $config['prev_tag_close']   = '</li>';
        $config['next_link']        = '&raquo';
        $config['next_tag_open']    = '<li class="page-item">';
        $config['next_tag_close']   = '</li>';
        $config['last_tag_open']    = '<li class="page-item">';
        $config['last_tag_close']   = '</li>';
        $config['cur_tag_open']     = '<li class="page-item active"><a class="page-link" href="#">';
        $config['cur_tag_close']    = '</a></li>';
        $config['num_tag_open']     = '<li>';
        $config['num_tag_close']    = '</li>';
        
        return $config;
    }
    
    function getaddress($lat,$lng)
  {
    $url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($lat).','.trim($lng).'&sensor=false&key=AIzaSyBPLaKp8iVdNP6lVdIeCnfpqQ_WH9E9gR4';
    $json = @file_get_contents($url);
    $data=json_decode($json);
    $status = $data->status;
    if($status=="OK")
      return $data->results[0]->formatted_address;
    else
      return false;
  }
  function acakId($string)
  {
    $s = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 3)), 0, 3);
    $tengah = substr(str_shuffle(str_repeat("0123456789", 2)), 0, 2);
    $akhir = substr(str_shuffle(str_repeat("0123456789", 5)), 0, 5);
    $hasilnya = $s.$tengah.$string.$akhir;
    return $hasilnya;
  }

}
