
    <div class="ks-column ks-page">
        <div class="ks-header">
            <section class="ks-title">
                <h3>Gudang</h3>

                <div class="ks-controls">
                    <button class="btn btn-primary-outline ks-light ks-filemanager-navigation-block-toggle" data-block-toggle=".ks-filemanager-page > .ks-navigation">Navigation</button>
                    <button class="btn btn-primary-outline ks-light ks-filemanager-info-block-toggle" data-block-toggle=".ks-filemanager-page > .ks-info">Info</button>
                </div>
            </section>
        </div>

        <div class="ks-content">
            <div class="ks-body ks-filemanager-page">
                <div class="ks-navigation ks-scrollable" data-auto-height>
    <div class="ks-wrapper">
        <div class="ks-separator">Tambah Baru</div>
            <ul class="ks-tree">
                <li class="ks-has-submenu">
                <a>
                    <form name="F1" id="F1">  
                        <div class="form-group">
                            <label for="subject">Nama Gudang</label>
                            <input type="text" class="form-control" id="warehouse_name" name="warehouse_name" placeholder="Nama Gudang">
                            <input type="hidden" name="warehouse_id" class="form-control" id="warehouse_id">
                            <input type="hidden" name="action" class="form-control" id="action" value="<?php echo $action; ?>">
                        </div>                   
                        <div class="form-group">
                            <label for="subject">Deskripsi</label>
                            <input type="text" class="form-control warehouse_description" id="warehouse_description" name="warehouse_description" placeholder="Deskripsi Gudang">
                        </div>                          
                        <div class="form-group">
                            <label for="subject">Nama Area</label>
                            <select class="form-control ks-select region_id" name="region_id" id="select2-id-label-single region_id">
                               <?php
                                    foreach ($region as $key) {
                                        # code...
                                        echo "<option value='".$key->region_id."'>".$key->region_name."</option>";
                                    }
                               ?>
                            </select>
                        </div>                              
                        <div class="form-group">
                            <label for="subject">Status Aktif</label>
                            <select class="form-control ks-select warehouse_active_status" name="warehouse_active_status" id="select2-id-label-single warehouse_active_status">
                                <option value="1">Aktif</option>
                                <option value="0">Tidak Aktif</option>
                            </select>
                        </div>                   
                        <div class="form-group">
                            <div class="ks-actions">
                                <button type="button" onclick="saveData()" class="btn btn-success">Kirim</button>
                                <input type="reset" class="btn btn-primary-outline ks-light ks-view-cancel" value="Batal"></input>
                                </button>
                            </div>
                        </div>
                    </form>
                </a>
            </li>
        </ul>
    </div>
</div>

<div class="ks-files">
    <div class="ks-header">
        <div class="ks-search input-icon icon-right icon icon-lg">
            <input id="input-group-icon-text" type="text" class="form-control cari" id="cari" placeholder="Cari...">
            <span class="icon-addon">
                <span class="fa fa-search" onclick="setData()"></span>
            </span>
        </div>
        <div class="ks-filters">
            <div class="ks-sortable btn-group">
                <button class="btn btn-primary-outline ks-light dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="btn-text-lighter">Urutkan</span>
                </button>
                <div class="dropdown-menu dropdown-menu-right ks-dropdown-menu-sortable urutkan">
                     <input name="sort_by" id="sort_by" type="hidden" value="warehouse_name">
                    <a class="dropdown-item dropdown-item-checked" href="#" id="region_name" onclick="urutkan('warehouse_name')">
                        <span class="ks-sort-by-text">Berdasarkan</span> Nama Gudang
                        <input name="radio" type="radio">
                    </a>
                    <a class="dropdown-item" href="#" id="region_name" onclick="urutkan('region_name')">
                        <span class="ks-sort-by-text">Berdasarkan</span> Area
                        <input name="radio" type="radio">
                    </a>
                    <a class="dropdown-item" href="#" id="warehouse_active_status" onclick="urutkan('warehouse_active_status')">
                        <span class="ks-sort-by-text">Berdasarkan</span> Status Aktif
                        <input name="radio" type="radio">
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="ks-content ks-scrollable" data-auto-height data-fix-height="40">
        <!--table-->
        <div class="card-block">
            <table class="table table-bordered vertical-align-middle tabelnya" id="tabelnya">
                <thead>
                <tr>
                    <th width="110">Nama Gudang</th>
                    <th width="160">Deskripsi</th>
                    <th width="20">Area</th>
                    <th width="30">Status Aktif</th>
                    <th>&nbsp;</th>
                </tr>
                </thead>
                <tbody id="tbody">
                <?php
                    foreach ($list as $key) {
                        # code...
                        switch ($key->warehouse_active_status) {
                        case '1':
                            # code...
                        $status = '<span class="badge badge-pill badge-success">Aktif</span>';
                        $status1 = "Non-Aktif";
                        $status_balik = "0";
                            break;
                        case '0':
                            # code...
                        $status = '<span class="badge badge-pill badge-danger">Non-Aktif</span>';
                        $status1 = "Aktif";
                        $status_balik = "1";
                            break;
                        }

                    echo '
                        <tr id="tr_'.$key->warehouse_id.'">
                    <td>
                        <div class="text-block">
                            <div class="text-block-container">
                                <div class="text-block-text">'.$key->warehouse_name.'</div>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="table-cell-block image">
                            <div class="text-block-container">
                                <div class="text-block-text">'.$key->warehouse_description.'</div>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="table-cell-block">
                            <div class="text-block-container">
                                <div class="text-block-text">'.$key->region_name.'</div>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="table-cell-block status">
                            <div class="status-block-container">
                            <span class="user_active_status_'.$key->warehouse_id.'">'.$status.'</span>
                            </div>
                        </div>
                    </td>        
                    <td class="table-actions">
                        <div class="dropdown">
                            <a class="btn btn-link" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="fa fa-ellipsis-h"></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                                <a class="dropdown-item" href="#" onClick=edit("'.base_url().'app/warehouse/edit/'.$key->warehouse_id.'")>
                                    <span class="fa fa-pencil icon text-primary-on-hover"></span> Edit
                                </a>
                                <a class="dropdown-item" href="#" onClick=changeStatus("'.base_url().'app/warehouse/change_status/'.$key->warehouse_id.'")><span class="fa fa-remove icon text-danger-on-hover "></span><span class="aksi_user_active_status_'.$key->warehouse_id.'">'.$status1.'</span></a>
                            </div>
                        </div>
                    </td>
                </tr>
                    ';

                    }
                ?>
                             
                </tbody>
            </table>

            <!--paging-->
            <div class="ks-items-block pages_paging" id="pages_paging"><nav>
                <?php echo $pagination; ?></nav>
            </div>
            <!--end paging-->
        </div>
        <!--end--> 
    </div>
</div>
<script type="text/javascript">



function changeStatus(url)
{
    $.ajax({
    type: "GET",
    url: url,
    cache: false,                  
    success:function(response) {
        console.log(response);
        var objJSON = JSON.parse(response);
        var id = objJSON.ID;
        var status_name = objJSON.STATUS_NAME;
        if (objJSON.STATUS_CODE == '1') {
            var revert_status_name = 'Non-Aktif';
        }else
        {
            var revert_status_name = 'Aktif';
        }
        $('.aksi_user_active_status_'+id).html(revert_status_name);
        if (objJSON.STATUS_CODE == '1') {
            $('.user_active_status_'+id).html('<span class="badge badge-pill badge-success">Aktif</span>');
        }else 
        {
            $('.user_active_status_'+id).html('<span class="badge badge-pill badge-danger">Non-Aktif</span>');
        }
    }
});
}
// fungsi tambah baru
function saveData()
{
    var url = $('#action').val();
        $.ajax({
    type: "post",
    url: url,
    cache: false,               
    data: $('#F1').serialize(),    
    success:function(response) {
      //console.log(response); 
      var objJSON = JSON.parse(response); 
      if (objJSON.RC != 999) {
            alert(objJSON.MESSAGE);
      }else 
      {
        //console.log(objJSON.RC);
            alert(objJSON.MESSAGE);
        setData();
        bersihkanForm();
       // setPaging();
      }    
    
    }
}); 
}
function bersihkanForm()
{
    $('#F1').trigger('reset')
}

// untuk menampilkan data ke tabel
function setData()
{
    var cari = $('.cari').val();
    var sort_by = $('#sort_by').val();
     $.ajax({
        type: "post",
        url: '<?php echo base_url()."app/warehouse/search";?>',
        data: {sort_by : sort_by, keyword : cari},
        success: function(res){
            console.log(res);
            var obj = JSON.parse(res);
        if (obj.RC == 999) {
           setContent(obj);
        }
        
        }
         
    });
 

}


function urutkan(sort_by)
{
    $('#sort_by').val(sort_by);
    $('.urutkan a').removeClass('dropdown-item-checked');
    var sort =  $('#sort_by').val();
    setData();
}
function edit(url)
{
  $('.tambah_baru').html('Edit Data');
  $.ajax({
    type: "GET",
    url: url,
    cache: false,                  
    success:function(response) {
        console.log(response);
        var objJSON = JSON.parse(response);

        var warehouse_id = objJSON.DATA.warehouse_id;
        var action = objJSON.ACTION;
        var warehouse_name = objJSON.DATA.warehouse_name;
        var region_name = objJSON.DATA.region_name;
        var region_id = objJSON.DATA.region_id;
        var warehouse_description = objJSON.DATA.warehouse_description;
        var region_active_status = objJSON.DATA.warehouse_active_status;
        var country_id = objJSON.DATA.region_id;
        

        $('#warehouse_name').val(region_name);
        $('#warehouse_id').val(warehouse_id);
        $('#warehouse_description').val(warehouse_description);        
        $('#region_id').val(region_id);       
        $('#action').val(action);
        $('#warehouse_active_status').val(region_active_status);
       

        var list_1 = objJSON.REGION;
        
        $('.warehouse_active_status').empty(); 
        $('.region_id').empty();

            if (objJSON.DATA.warehouse_active_status != 0) {            
            $('.warehouse_active_status').append("<option value='1'>Aktif</option>"); 
            $('.warehouse_active_status').append("<option value='0'>Tidak Aktif</option>");
        }else 
        {
            $('.warehouse_active_status').append("<option value='0'>Tidak Aktif</option>");
            $('.warehouse_active_status').append("<option value='1'>Aktif</option>"); 
        } 

        $('.region_id').append("<option value='"+region_id+"'>"+region_name+"</option>");
           $.each(list_1, function(key, value) {
           if (value.region_id != region_id) {            
            $('.region_id').append("<option value='"+value.region_id+"'>"+value.region_name+"</option>"); 
           }                  
        });


        
    }
});

   
}

function setContent(obj)
{
    var content = '';
    
           for (i = 0; i < obj.MESSAGE.length; i++) {
            if(obj.MESSAGE[i].warehouse_active_status == '1') {
                var status = '<span class="badge badge-pill badge-success">Aktif</span>'; var status1 = 'Non-Aktif';}else {var status = '<span class="badge badge-pill badge-danger">Non-Aktif</span>';
            var status1 = 'Aktif'; }

                content += '<tr id="tr_'+obj.MESSAGE[i].warehouse_id+'">';
                content += ' <td><div class="text-block"><div class="text-block-container"><div class="text-block-text">'+obj.MESSAGE[i].warehouse_name+'</div></div></div></td>';

                content += '<td><div class="text-block image"><div class="text-block-container"><div class="text-block-text">'+obj.MESSAGE[i].warehouse_description+'</div></div></div></td>';

                content += '<td><div class="table-cell-block"><div class="text-block-container"><div class="text-block-text">'+obj.MESSAGE[i].region_name+'</div></div></div></td>';
                
                content += '<td><div class="table-cell-block status"><div class="status-block-container"><span class="user_active_status_'+obj.MESSAGE[i].warehouse_id+'">'+status+'</span></div></div></td> ';
                
                content += '<td class="table-actions"><div class="dropdown"><a class="btn btn-link" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="fa fa-ellipsis-h"></span></a><div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1"><a class="dropdown-item" href="#" onClick=edit("<?php echo base_url();?>app/warehouse/edit/'+obj.MESSAGE[i].warehouse_id+'")><span class="fa fa-pencil icon text-primary-on-hover"></span> Edit</a><a class="dropdown-item" href="#" onClick=changeStatus("<?php echo base_url();?>app/warehouse/change_status/'+obj.MESSAGE[i].warehouse_id+'")><span class="fa fa-remove icon text-danger-on-hover "></span><span class="aksi_user_active_status_'+obj.MESSAGE[i].warehouse_id+'">'+status1+'</span></a></div></div> </td>';
                content += '</tr>';
                            
            }
            //console.log(content);
            $('#tbody').html(content);
            $('#pages_paging').html(obj.paging);
}
function toRp(a,b,c,d,e){e=function(f){return f.split('').reverse().join('')};b=e(parseInt(a,10).toString());for(c=0,d='';c<b.length;c++){d+=b[c];if((c+1)%3===0&&c!==(b.length-1)){d+='.';}}return'Rp.\t'+e(d)+',00'}


</script>
