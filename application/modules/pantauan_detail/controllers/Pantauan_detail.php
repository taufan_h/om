<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pantauan_detail extends CI_Controller {

public $client_id;
public $user_id;
    function __construct() {
       parent::__construct();
       $this->cek_session();
       $this->load->model('m_pantauan_detail','model');
       $this->client_id   = $this->session->userdata('client_id');  
       $this->user_id   = $this->session->userdata('user_id'); 
    }


 function cek_session()
    {       
        if($this->session->userdata("status_login") != true)
        {
           $this->session->set_flashdata('msg_login', 'Anda harus login terlebih dahulu.');
            redirect(base_url());
        }
    }

    function pantauan_view($idnya, $offset = null)
    {
      $id1 = substr($idnya,5);
      $id = substr($id1, 0,-5);    
      
        $list = array();
        $photo = array();
        if ($this->model->select_dt($id)->num_rows() > 0) {
          # code...
          $RC = 200;
        }else
        {
          $RC = 404;
        }

        if ($RC == 200) {
          # code... 
          $lst = $this->model->select_dt($id)->result();
          foreach ($lst as $key) {
            # code...
            $mitra_latlng = $this->getLatLng($key->mitra_address);
            if ($mitra_latlng != '0') {
              # code...
              $expl = explode('#', $mitra_latlng);
              $expl_lat = $expl[0];
              $expl_lng = $expl[1];
            }else
            {
              $expl_lat = '0';
              $expl_lng = '0';
            }

            $param['user_id']         = $key->user_id;
            $param['mitra_name']      = $key->mitra_name;
            $param['activity_result_battery_level']         = $key->activity_result_battery_level;
            $param['user_fullname']         = $key->user_fullname;
            $param['mitra_email']         = $key->mitra_email;
            $param['activity_result_notes']         = $key->activity_result_notes;
            $param['activity_result_id']         = $key->activity_result_id;
            $param['lokasi_tujuan_lat']         = $expl_lat;
            $param['lokasi_tujuan_lng']         = $expl_lng;
            $param['mitra_address']         = $key->mitra_address;
            $param['lokasi_terdeteksi']         = $this->getaddress($key->activity_result_lat_in,$key->activity_result_lng_in);
            $param['activity_result_lat_in']    = $key->activity_result_lat_in;
            $param['activity_result_lng_in']    = $key->activity_result_lng_in;

            array_push($list, $param);
          }

          $list_photo = $this->model->select_photo($id)->result();
 foreach ($list_photo as $key) {
            # code...
            $param['activity_result_id']         = $key->activity_result_id;
            $param['activity_file_real_name']      = $key->activity_file_real_name;
            $param['activity_file_date']         = $key->activity_file_date;
            $param['activity_file_lat']         = $key->activity_file_lat;
            $param['activity_file_lng']         = $key->activity_file_lng;
            $param['activity_fake_gps']         = $key->activity_fake_gps;
            $param['lokasi_terdeteksi']         = $this->getaddress($key->activity_file_lat,$key->activity_file_lng);

            array_push($photo, $param);
          }

        }
        //$dat = json_encode(array('DATA' => $list));
        $dat = json_encode($list);

       $data['RC'] = $RC;
       $data['ID'] = $idnya;
       $data['LIST'] = $dat;
       $data['PHOTO'] = json_encode($photo);
       if ($RC == 200) {
         # code...
        $this->template->front('pantauan_detail/view_valid',$data);
       }else
       {
        $this->template->front('pantauan_detail/view_invalid',$data);
       }
    // print_r($data['LIST']);
        
    }


    
    function getaddress($lat,$lng)
  {
    $url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($lat).','.trim($lng).'&sensor=false&key='.key_googleapis;
    $json = @file_get_contents($url);
    $data=json_decode($json);
    $status = $data->status;
    if($status=="OK")
      return $data->results[0]->formatted_address;
    else
      return "Error";
  }
  function getLatLng($address)
  {
    $address = str_replace(' ', '-', $address);
    $url = 'https://maps.googleapis.com/maps/api/geocode/json?address='.trim($address).'&sensor=false&key='.key_googleapis;
    $json = @file_get_contents($url);
    $data=json_decode($json);
    $status = $data->status;
    if($status=="OK")
      return $data->results[0]->geometry->location->lat."#".$data->results[0]->geometry->location->lng;
    else
      return "0";
  }

}
