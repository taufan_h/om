<?php

class M_Pantauan_detail extends CI_Model {
    //put your code here
    public $client_id;
    public $user_id;
    public function __construct() {
        parent::__construct();

        $this->user_id = $this->session->userdata('user_id');
        $this->client_id = $this->session->userdata('client_id');
    }
    
    function select_dt( $id)
    {
        $result = $this->db->query("select * FROM tbl_activity_result  
                    inner join tbl_user using (user_id)    
                    inner join tbl_mitra on tbl_mitra.mitra_id = tbl_activity_result.mitra_id       
                    WHERE tbl_activity_result.client_id = '".$this->client_id."' && activity_result_id = '".$id."'");
        return $result;
    }
     function select_photo($id)
    {
        $result = $this->db->query("select * FROM tbl_activity_file  
                    inner join tbl_user using (user_id)    
                    inner join tbl_activity_result on tbl_activity_result.activity_result_id = tbl_activity_file.activity_result_id       
                    WHERE tbl_activity_file.client_id = '".$this->client_id."' && tbl_activity_result.activity_result_id = '".$id."'");
        return $result;
    }
    
    function ambil_data($tbl,$field,$id)
    {
        $result = $this->db->query("select * FROM tbl_activity_result       
                    WHERE tbl_activity_result.client_id = '".$this->client_id."' 
                    && $field  = '".$id."'");
        return $result;
    }
    function select_data($table)
    {
        $this->db->select('*');
        return $this->db->get($table);
    }
    
    function total_dt($date)
    {
        $result = $this->db->query("select * FROM tbl_activity_result         
                    WHERE client_id = '".$this->client_id."' && date(activity_check_in_date) = '".$date."' ");
        return $result;
    }
    
    function cek_nama($table, $field, $record)
    {
        $this->db->select('*')
                ->where($field,$record)
                ->where('client_id',$this->client_id);
                
        $sql = $this->db->get($table);
        return $sql;
                
    }
     function cek_nama_not_in($table, $field_where,$field_not_id, $record,$user_id)
    {
        $this->db->select('*')
                ->where($field_where,$record)
                ->where_not_in($field_not_id,$user_id)
                ->where('client_id',$this->client_id);
                
        $sql = $this->db->get($table);
        return $sql;
                
    }
    function select_where($table, $where)
    {
        $this->db->select('*')
                ->where($where)
                ->from($table);
        return $this->db->get();
               
    }
   
   function cek_id($id)
   {
    $this->db->select('*')
    ->where('client_id',$this->client_id)
    ->where('activity_result_id',$id)
    ->from('tbl_activity_result');

    return $this->db->get();
   }
   
    
}
