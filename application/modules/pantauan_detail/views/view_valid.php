<div class="ks-column ks-page">
        <div class="ks-header">
            <section class="ks-title">
                <h3>Detail Hasil Pantauan</h3>

                <div class="ks-controls">
                    <button class="btn btn-primary-outline ks-light ks-filemanager-navigation-block-toggle" data-block-toggle=".ks-filemanager-page > .ks-navigation">Navigation</button>
                    <button class="btn btn-primary-outline ks-light ks-filemanager-info-block-toggle" data-block-toggle=".ks-filemanager-page > .ks-info">Info</button>
                </div>
            </section>
        </div>

        <div class="ks-content">
            <div class="ks-body ks-filemanager-page">


<div class="ks-files">
    <div class="ks-content ks-scrollable" data-auto-height data-fix-height="40">
   <div id="map"></div>
        <!--table-->
        <div class="card-block">
            <table class="table table-bordered vertical-align-middle">
                <thead>
                <tr>
                    <th width="110">Nama Pengguna</th>
                    <th width="10%">Nama Mitra</th>
                    <th width="160">Lokasi Tujuan</th>
                    <th width="20">Lokasi Terdeteksi</th>
                    <th width="80" style="text-align: center">Baterai</th>
                    <th width="20">Catatan</th>
                </tr>
                </thead>
                <tbody>
                <tbody id="tbody">
                <?php
                    foreach (json_decode($LIST) as $key) {
                        if ($key->activity_result_battery_level < 25) {
                            # code...
                            $battery = '<span class="badge badge-pill badge-danger">'.$key->activity_result_battery_level.' %</span>';
                        }else
                        if ($key->activity_result_battery_level >= 25 && $key->activity_result_battery_level < 50) 
                        {
                            $battery = '<span class="badge badge-pill badge-warning">'.$key->activity_result_battery_level.' %</span>';
                        }else
                        if ($key->activity_result_battery_level >= 50 ) 
                        {
                            $battery = '<span class="badge badge-pill badge-success">'.$key->activity_result_battery_level.' %</span>';
                        }
                        # code...
                        echo '
                        <tr>
                    <td>
                        <div class="text-block">
                            <div class="text-block-container">
                                <div class="text-block-text">'.$key->user_fullname.'</div>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="text-block">
                            <div class="text-block-container">
                                <div class="text-block-text">'.$key->mitra_name.'</div>
                                <div class="text-block-sub-text">'.$key->mitra_email.'</div>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="table-cell-block image">
                            <div class="text-block-container">
                                <div class="text-block-text">'.$key->mitra_address.'</div>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="table-cell-block image">
                            <div class="text-block-container">
                                <div class="text-block-text">'.$key->lokasi_terdeteksi.'</div>
                            </div>
                        </div>
                    </td>                    
                    <td style="text-align: center">
                        <div class="table-cell-block">
                            <div class="text-block-container">'.$battery.'
                            </div>
                        </div>
                    </td>                                      
                    <td>
                        <div class="table-cell-block">
                            <div class="text-block-container">
                                <div class="text-block-text">'.$key->activity_result_notes.'</div>
                            </div>
                        </div>
                    </td>        
                </tr>';
                    }
                ?>             
                </tbody>
            </table>


            <table class="table table-bordered vertical-align-middle">
                <thead>
                <tr>
                    <th width="110">Foto</th>
                    <th width="20">Lokasi Foto</th>
                    <th width="80" style="text-align: center">GPS Palsu?</th>
                </tr>
                </thead>
                <tbody>
                <?php
                    if (count(json_decode($PHOTO)) > 0) {
                        # code...
                        foreach (json_decode($PHOTO) as $key) {
                        # code...
                        if ($key->activity_fake_gps == 0) {
                            # code...
                            $gps = '<span class="badge badge-pill badge-success">Tidak</span>';
                        }else
                        {
                            $gps = '<span class="badge badge-pill badge-danger">Ya</span>';
                        }

                        echo '
                            <tr>
                    <td>
                        <div class="text-block">
                            <div class="text-block-container">
                                <div class="text-block-text"><img src="'.includes_url.'uploads/file/'.$key->activity_file_real_name.'" width="80"></div>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="table-cell-block image">
                            <div class="text-block-container">
                                <div class="text-block-text">'.$key->lokasi_terdeteksi.'</div>
                            </div>
                        </div>
                    </td>                   
                    <td style="text-align: center">
                        <div class="table-cell-block">
                            <div class="text-block-container">
                                '.$gps.'
                            </div>
                        </div>
                    </td>                                              
                </tr>
                        ';
                    }
                    }else
                    {
                        echo '<tr><td colspan="3"><center><h4>Tidak Ada Photo yang Diupload</h4></center></tr>';
                    }
                ?>               
                </tbody>
            </table>
        </div>
        <!--end--> 
    </div>
</div>
 <script type="text/javascript">

 function initMap() {
       <?php $data = json_decode($LIST);?>
var bounds = new google.maps.LatLngBounds();
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 5,
          center: new google.maps.LatLng('-6.1902078', '106.87938071')
        });

    var tujuan = new google.maps.Marker({
        position: new google.maps.LatLng('<?php echo $data[0]->lokasi_tujuan_lat; ?>', '<?php echo $data[0]->lokasi_tujuan_lng; ?>'),
        map: map
    });
    var terdetek = new google.maps.Marker({
        position: new google.maps.LatLng('<?php echo $data[0]->activity_result_lat_in; ?>', '<?php echo $data[0]->activity_result_lng_in; ?>'),
        map: map
    });
    bounds.extend(terdetek.position);

    google.maps.event.addListener(tujuan, 'click', function() {
        infowindow = new google.maps.InfoWindow({
            content: '<?php echo $data[0]->mitra_address; ?>'
        });
        infowindow.open(map, tujuan);
    });
    google.maps.event.addListener(terdetek, 'click', function() {
        infowindow = new google.maps.InfoWindow({
            content: '<?php echo $data[0]->lokasi_terdeteksi; ?>'
        });
        infowindow.open(map, terdetek);
    });
    map.fitBounds(bounds);
    google.maps.event.addListenerOnce(map, 'bounds_changed', function(event) {
        if (this.getZoom() > 15) {
            this.setZoom(12);
        }
    });

}      
                                                
</script>
