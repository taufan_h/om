 <div class="ks-column ks-page">
        <div class="ks-header">
            <section class="ks-title">
                <h3>Profil</h3>
                <div class="ks-controls">
                    <button type="button" class="btn btn-primary-outline ks-light ks-profile-tabs-block-toggle" data-block-toggle=".ks-profile > .ks-tabs-container">Tabs</button>
                    <button type="button" class="btn btn-primary-outline ks-light ks-settings-menu-block-toggle" data-block-toggle=".ks-settings-tab > .ks-menu">Menu</button>
                </div>
            </section>
        </div>

        <div class="ks-content">
            <div class="ks-body ks-profile">
                <div class="ks-header">
                    <div class="ks-user">
                        <img src="<?php echo base_url().'includes/uploads/avatar/'.$this->session->userdata('user_photo'); ?>" class="ks-avatar" width="100" height="100" id="ava2">
                        <div class="ks-info">
                            <div class="ks-name"><?php echo $list->user_fullname; ?></div>
                            <div class="ks-description"><?php echo $list->mitra_name; ?></div>
                        </div>
                    </div>
                </div>
                <div class="ks-tabs-container ks-tabs-default ks-tabs-no-separator ks-full ks-light">
                    <div class="tab-content">
                        <div class="tab-pane active" id="settings" role="tabpanel" aria-expanded="false">
                            <div class="ks-settings-tab">
                                <form class="ks-form ks-general"> <!-- ks-uppercase ks-light -->
                                    <h3 class="ks-header">
                                        Pengaturan
                                    </h3>

                                    <div class="ks-manage-avatar ks-group">
                                        <div class="ks-body">
                                            <div class="ks-header">Ganti foto profil</div>
                                            <div class="ks-description">
                                                Foto disarankan ukuran 100x100 pixel
                                            </div>
                                            <div class="ks-controls"><input type="file" name="file_image" id="file_image"></input>
                                                <button type="button" onclick="uploadPhoto()" class="btn btn-primary">
                                                    <span class="fa fa-upload ks-icon"></span>
                                                    <span class="ks-text">Unggah Foto</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="ks-group ks-linked-accounts">
                                        <div class="ks-header">Ubah Kata Sandi</div>
                                        
                                            <div class="col-lg-3">
                                            <div class="form-group">
                                                <label for="subject">Kata Sandi saat ini</label>
                                                <input type="password" class="form-control" name="password_old" id="password_old" placeholder="Kata Sandi/Password saat ini">
                                            </div> 
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label for="subject">Kata Sandi baru</label>
                                                <input type="password" class="form-control" name="password_new" id="password_new" placeholder="Kata Sandi/Password baru">
                                            </div> 
                                        </div>
                                        
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <div class="ks-actions">
                                                    <button type="button" onclick="ubahPassword()" class="btn btn-success">Ubah</button>
                                                </div>
                                            </div>    
                                        </div>   
                                    </div>
                                    <div class="ks-group ks-connect-with-social-accounts">
                                        <div class="ks-header">Ubah Nama Pengguna</div>
                                        <div class="ks-body">
                                            
                                                <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label for="subject">Nama Pengguna Anda</label>
                                                    <input type="text" class="form-control" name="user_username" id="user_username" placeholder="<?php echo $list->username; ?>">
                                                </div> 
                                         
                                            </div>
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <div class="ks-actions">
                                                        <button type="button" onclick="ubahUsername()" class="btn btn-success">Ubah</button>
                                                    </div>
                                                </div>    
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



<script type="text/javascript">

function ubahUsername()
{
    var username = $('#user_username').val();
   
     var url = '<?php echo base_url()."app/profil/username";?>';
       if (username == "") {
        alert("Mohon Isi Nama Pengguna");
       }else 
       {
         $.ajax({
            type: "post",
            url: url,
            cache: false,               
            data: {username : username},    
            success:function(response) {
            console.log(response); 
            var objJSON = JSON.parse(response); 
            if (objJSON.RC == 999) {
                     $('#user_username').val('');
            }   
            alert(objJSON.MESSAGE);
            }
        }); 
       }

}

function ubahPassword()
{
    var password_old = $('#password_old').val();
    var password_new = $('#password_new').val();
   
     var url = '<?php echo base_url()."app/profil/password";?>';
       if (password_old == "" || password_new == "") {
        alert("Mohon Isi password");
       }else 
       {
         $.ajax({
            type: "post",
            url: url,
            cache: false,               
            data: {password_old : password_old, password_new : password_new},    
            success:function(response) {
            console.log(response); 
            var objJSON = JSON.parse(response); 
            if (objJSON.RC == 999) {
                     $('#password_old').val('');
                     $('#password_new').val('');
            }   
            alert(objJSON.MESSAGE);
            }
        }); 
       }

}

function uploadPhoto()
{
     var data = new FormData();
    data.append('userfile', $('#file_image').prop('files')[0]);
    // append other variables to data if you want: data.append('field_name_x', field_value_x);

    if($('#file_image').val() != '')
    {
        $.ajax({
        type: 'POST',               
        processData: false, // important
        contentType: false, // important
        data: data,
        url: '<?php echo base_url()."app/profil/upload";?>',
        dataType : 'json',  
        // in PHP you can call and process file in the same way as if it was submitted from a form:
        // $_FILES['file_image']
         success:function(response) {
            //console.log(response); 
             console.log(response);
            //var obj = JSON.parse(response);
                if (response.RC == 999) {        
                     $('#file_image').val('');
                     $('#ava').attr('src', '<?php echo base_url()."includes/uploads/avatar/";?>'+response.PHOTO);
                     $('#ava2').attr('src', '<?php echo base_url()."includes/uploads/avatar/";?>'+response.PHOTO);
                    alert("Upload Sukses");
                }else 
                {
                    alert("Upload Gagal");

                }   
            }
        
        }); 
    }else 
    {
        alert('Mohon Pilih Gambar');
    }
}


</script>
