<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profil extends CI_Controller {

public $client_id;
public $user_id;
    function __construct() {
       parent::__construct();
       $this->cek_session();
       $this->load->model('m_profil','model');
       $this->client_id   = $this->session->userdata('client_id');  
       $this->user_id   = $this->session->userdata('user_id');  
    }


 function cek_session()
    {       
        if($this->session->userdata("status_login") != true)
        {
           $this->session->set_flashdata('msg_login', 'Anda harus login terlebih dahulu.');
            redirect(base_url());
        }
    }

    function profil_view()
    {
      $data['list']      = $this->model->ambil_data()->row();
      $this->template->front('profil/view',$data);
        
    }
  
 
    function change_username()
    {       
      $username              = $this->input->post('username');
      $data = array(
          'username'             => $username
      );
      
      $cek = $this->model->cek_nama_not_in('tbl_user','username','user_id',$username,$this->user_id)->num_rows();
      if($cek >0)
       {
        $json_e  = array('RC' =>'998','MESSAGE' => 'nama pengguna sudah ada');
       }else{
          $this->model->update_data('tbl_user','user_id',$this->user_id,$data);
        $json_e  = array('RC' =>'999','MESSAGE' => 'Data Berhasil diedit');
            
       }
      echo json_encode($json_e);
    }

    function change_password()
    {
          $password_new = $this->input->post('password_new');
          $password_old = $this->input->post('password_old');

          $acak_password_new = $this->acak_password($password_new);
          $acak_password_old = substr($this->acak_password($password_old), 5);

          $cek_password_old = $this->model->select_where('tbl_user','user_id',$this->user_id)->row();
          $ambil_passwd = substr($cek_password_old->user_password,5);

          if($ambil_passwd != $acak_password_old)
       {
        $json_e  = array('RC' =>'998','MESSAGE' => 'Password sekarang salah');
       }else{
         $data = array(
          'user_password'             => $acak_password_new
      );
          $this->model->update_data('tbl_user','user_id',$this->user_id,$data);
      
        $json_e  = array('RC' =>'999','MESSAGE' => 'Password Berhasil Diubah');
            
       }
      echo json_encode($json_e);


    }
  
   function upload_photo()
    {

      $get_img = $this->model->select_where('tbl_user','user_id',$this->user_id)->row();
      $img = $get_img->user_photo;
      $new_name = date('his')."_".$_FILES['userfile']['name'];
      $config = array(
        'upload_path' => "./includes/uploads/avatar/",
        'allowed_types' => "jpg|png|jpeg",
        'overwrite' => TRUE,
        'file_name' => $new_name,
        'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
       // 'max_height' => "768",
       // 'max_width' => "1024"
      );
      $this->load->library('upload', $config);
      

       if($this->upload->do_upload())
      {
        $data = array(
          'user_photo' => $new_name
          );
        $this->model->update_data('tbl_user','user_id',$this->user_id, $data);
         $data_session = array(  
                'user_photo'        => $new_name
            );
            $this->session->set_userdata($data_session);
            unlink('./includes/uploads/avatar/'.$img);
              $json_e  = array('RC' =>'999','PHOTO' => $new_name);
    
      }
      else
      {
              $json_e  = array('RC' =>'998');
         
      }  
      echo json_encode($json_e);
      
    }

      function acak_password($password)
      {
        $replacements = array('1' => '2',
                      '2' => '3', 
                      '3' => '4',
                      '4' => '5',
                      '5' => '6',
                      'a' => 'b',
                      'b' => 'c',
                      'c' => 'd',
                     );
        $capcay             = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxysABCDEFGHIJKLMNOPQRSTUVWXYZ", 5)), 0, 5);
        $acak               = substr($capcay,0,6);
        $txt_password2      = $acak.md5($password);
        $txt_new_password   = strtr($txt_password2,$replacements);

        return $txt_new_password;
      }

 
    

}
