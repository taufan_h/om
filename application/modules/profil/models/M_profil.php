<?php

class M_profil extends CI_Model {
    //put your code here
    public $client_id;
    public $user_id;
    public function __construct() {
        parent::__construct();

        $this->user_id = $this->session->userdata('user_id');
        $this->client_id = $this->session->userdata('client_id');
    }
    
      
    function ambil_data()
    {
        $result = $this->db->query("select * FROM tbl_user  
                    inner join tbl_mitra using (mitra_id)        
                    WHERE tbl_user.user_id = '".$this->user_id."'");
        return $result;
    }
    function select_data($table)
    {
        $this->db->select('*');
        return $this->db->get($table);
    }
    
    
    function cek_nama($table, $field, $record)
    {
        $this->db->select('*')
                ->where($field,$record)
                ->where('client_id',$this->client_id);
                
        $sql = $this->db->get($table);
        return $sql;
                
    }
     function cek_nama_not_in($table, $field_where,$field_not_id, $record,$user_id)
    {
        $this->db->select('*')
                ->where($field_where,$record)
                ->where_not_in($field_not_id,$user_id)
                ->where('client_id',$this->client_id);
                
        $sql = $this->db->get($table);
        return $sql;
                
    }
    function select_where($table, $field, $record)
    {
        $this->db->select('*')
                ->where($field,$record)
                ->from($table);
        return $this->db->get();
               
    }
   
    function update_data($table,$field,$record,$data)
    {
        $this->db->where($field, $record);
        $sql = $this->db->update($table, $data); 
        return $sql;
    }

   
    
}
