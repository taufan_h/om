<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang extends CI_Controller {

public $client_id;
    function __construct() {
       parent::__construct();
       $this->cek_session();
       $this->load->model('m_barang','model');
       $this->client_id   = $this->session->userdata('client_id');  
    }


 function cek_session()
    {       
        if($this->session->userdata("status_login") != true)
        {
           $this->session->set_flashdata('msg_login', 'Anda harus login terlebih dahulu.');
            redirect(base_url());
        }
    }

    function barang_view($offset = null)
    {
        $sort_by = $this->input->get('sort_by');
        if(!$sort_by || $sort_by == '-') {$sort_by = 'product_name';}
        if($offset == null) {$offset = 0;}
        $limit = 20;     
        
        $data['list']      = $this->model->select_dt($sort_by, $offset, $limit)->result();
        $total_data = $this->model->total_dt()->num_rows();   
       
        $this->pagination->initialize($this->config_paging($limit, $total_data,"load"));
       
        $data['action'] = base_url().'app/barang/add/proccess';
        $data['pagination'] = $this->pagination->create_links();
        $data['list_unit'] = $this->model->select_data('tbl_unit')->result();
        $data['list_brand'] = $this->model->select_where('tbl_brand', 'client_id', $this->client_id)->result();
     
      $arr = array();
      $config['ul_class']='treeview';
      $config['client_id']=$this->client_id;
      $config['table']='tbl_category';
      $this->load->library('tree',$config);

    foreach ($this->model->select_where('tbl_category','client_id',$this->client_id)->result() as $key) {
        if($key->category_parent_id == 0)
        {
          $aa['category_id'] = $key->category_id;
          $aa['category_name'] = $key->category_name;
          $aa['category_active_status'] = $key->category_active_status;
          array_push($arr,$aa);
        }else
        {
         // $s = $this->model->select_where('tbl_category','category_parent_id',$key->category_id)->row();
          $aa['category_id'] = $key->category_id;
          $aa['category_name'] = $this->tree->getTree($key->category_id);
          $aa['category_active_status'] = $key->category_active_status;
          array_push($arr,$aa);
        }
      }
    $json_e = array('RC' =>'200', 'MESSAGE' => $arr );    
    $data['list_category'] = json_encode($json_e);
  

      $this->template->front('barang/view',$data);
        
    }


  
  
     function search($offset = null)
    {
      $sort_by = $this->input->post('sort_by');
      $keyword = $this->input->post('keyword');

       // $sort_by = 'mitra_active_status';
       // $keyword = '';

        if(!$sort_by || $sort_by == '-') 
          {
            $sort_by = 'product_name';

          }
          if ($sort_by == 'product_active_status') {
            # code...
             $by = 'desc';
          }else
          {
             $by = 'asc';
          }
        if($offset == null) {$offset = 0;}
        $limit = 20;
       
       $arr = array();
      $config['ul_class']='treeview';
      $config['client_id']=$this->client_id;
      $config['table']='tbl_category';
      $this->load->library('tree',$config);

    foreach ($this->model->select_where('tbl_category','client_id',$this->client_id)->result() as $key) {
        if($key->category_parent_id == 0)
        {
          $aa['category_id'] = $key->category_id;
          $aa['category_name'] = $key->category_name;
          $aa['category_active_status'] = $key->category_active_status;
          array_push($arr,$aa);
        }else
        {
         // $s = $this->model->select_where('tbl_category','category_parent_id',$key->category_id)->row();
          $aa['category_id'] = $key->category_id;
          $aa['category_name'] = $this->tree->getTree($key->category_id);
          $aa['category_active_status'] = $key->category_active_status;
          array_push($arr,$aa);
        }
      }

        
        $list      = $this->model->select_search($sort_by,$by, $offset, $limit, $keyword)->result();
        $total_data = $this->model->total_search($keyword)->num_rows();   
       
      $this->pagination->initialize($this->config_paging($limit, $total_data,"search"));
        $data['sort_by']       = $sort_by;
        $data['search']     = $keyword;
        $data['pagination'] = $this->pagination->create_links();
       $arr = array('RC' => 999, 'MESSAGE' =>  $list, 'category' => $arr, 'search' => $data['search'], 'total' => $total_data, 'paging' => $data['pagination']);
       $json = json_encode($arr);
       echo $json; 
     
        
            //$this->load->view('user/admin_view_user',$data);
        
    }
    
            
    function config_paging($limit, $total_data,$type)
    {
     
        $config['base_url']     = site_url().'app/barang/search/';
      
        $config['total_rows']   = $total_data;
        $config['per_page']     = $limit;
        $config["uri_segment"]  = 4;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"]    = floor($choice);
        //$config['page_query_string'] = TRUE;
        //config for bootstrap pagination class integration
        $config['attributes'] = array('class' => 'page-link');
        $config['full_tag_open']    = '<ul class="pagination">';
        $config['full_tag_close']   = '</ul>';
        $config['first_link']       = false;
        $config['last_link']        = false;
        $config['first_tag_open']   = '<li class="page-item">';
        $config['first_tag_close']  = '</li>';
        $config['prev_link']        = '&laquo';
        $config['prev_tag_open']    = '<li class="prev">';
        $config['prev_tag_close']   = '</li>';
        $config['next_link']        = '&raquo';
        $config['next_tag_open']    = '<li class="page-item">';
        $config['next_tag_close']   = '</li>';
        $config['last_tag_open']    = '<li class="page-item">';
        $config['last_tag_close']   = '</li>';
        $config['cur_tag_open']     = '<li class="page-item active"><a class="page-link" href="#">';
        $config['cur_tag_close']    = '</a></li>';
        $config['num_tag_open']     = '<li>';
        $config['num_tag_close']    = '</li>';
        
        return $config;
    }
    
    function add_proccess()
    {
        
        $product_name           	= $this->input->post('product_name');
        $product_code           	= $this->input->post('product_code');
        $category_id           		= $this->input->post('category_id');
        $product_color           	= $this->input->post('product_color');
        $brand_id           		= $this->input->post('brand_id');
        $product_height           		= $this->input->post('product_height');
        $unit_id           		= $this->input->post('unit_id');
        $product_min_purchase           		= $this->input->post('product_min_purchase');
        $product_active_status      = $this->input->post('product_active_status');
        $product_photo_upload = '';
        if (count($_FILES['userfile']['name']) > 0) {
        	# code...
        	 $nama_file = array();

        	 for($k = 0; $k < count($_FILES['userfile']['name']) ; $k++)
        	 {
        	 	$new_name = date('his')."_".str_replace('', "_", $_FILES['userfile']['name']);
        		$product_photo_upload .= $new_name.',';

        	 	array_push($nama_file, $new_name);
        	 }
		     
		     $product_photo_upload = substr($product_photo_upload, 0,-1);
		      $this->load->library('MY_Upload');
		      $this->upload->initialize(array(
				"file_name"		=> $nama_file,
				'upload_path' => "./includes/uploads/product_photo/"
			));
	
	//Perform upload.
	$this->upload->do_multi_upload("userfile");

      
        }


        $data = array(
            'client_id'             => $this->client_id,
            'product_name'         => $product_name,
            'product_code'         => $product_code,
            'category_id'             => $category_id,
            'product_color'         => $product_color,
            'brand_id'             => $brand_id,
            'product_photo'			=> $product_photo_upload,
            'product_height'         => $product_height,
            'unit_id'         => $unit_id,
            'product_min_purchase'         => $product_min_purchase,
            'product_active_status'              => $product_active_status
        );
       
        if($product_name == '' || $product_code == '' || $product_color == '' || $product_min_purchase == '')
        {
        	$json_e  = array('RC' =>'998','MESSAGE' => 'kolom Harus Diisi');
        }else{
           $this->model->insert_data('tbl_product',$data);  

     		$json_e  = array('RC' =>'999','MESSAGE' => 'Sukses Tambah Data');
        }
        echo json_encode($json_e);
       
    }
    
    function edit($id)
    {      
        $get                    = $this->model->ambil_data('tbl_category','category_id',$id)->row();
        $config['ul_class']='treeview';
        $config['client_id']=$this->client_id;
        $config['table']='tbl_category';
        $this->load->library('tree',$config);

        $arr = array();
        foreach ($this->model->select_where('tbl_category','client_id',$this->client_id)->result() as $key) {
            if($key->category_parent_id == 0)
            {
              $aa['category_id'] = $key->category_id;
              $aa['category_name'] = $key->category_name;
              $aa['category_active_status'] = $key->category_active_status;
              array_push($arr,$aa);
            }else
            {
             // $s = $this->model->select_where('tbl_category','category_parent_id',$key->category_id)->row();
              $aa['category_id'] = $key->category_id;
              $aa['category_name'] = $this->tree->getTree($key->category_id);
              $aa['category_active_status'] = $key->category_active_status;
              array_push($arr,$aa);
            }
          }
      $json_e = array('RC' =>'200', 'MESSAGE' => $arr );    
     // $data['list_category'] = json_encode($json_e);
      $data['action']    = 'kategori/edit/proccess';

        //$this->load->view('user/formodel',$data);
        echo json_encode(array('RC' => '999', 'DATA' => $get,'ACTION' => $data['action'],'CATEGORY' =>$arr ));
    }
    function edit_proccess()
    {
         
       
        $category_id              = $this->input->post('category_id');       
        $category_name           = $this->input->post('category_name');
        $category_parent_id           = $this->input->post('category_parent_id');
        $category_active_status              = $this->input->post('category_active_status');

        $data = array(
            'client_id'             => $this->client_id,
            'category_name'         => $category_name,
            'category_parent_id'         => $category_parent_id,
            'category_active_status'              => $category_active_status
        );
        
        if($category_name == '')
        {
        $json_e  = array('RC' =>'998','MESSAGE' => 'Nama Kategori Harus Diisi', 'URL' => base_url().'app/kategori');
        }else{
            $this->model->update_data('tbl_category','category_id',$category_id,$data);
        $json_e  = array('RC' =>'999','MESSAGE' => 'Data Berhasil diedit', 'URL' => base_url().'app/kategori');
            
        }
        echo json_encode($json_e);
    }
    function change_status($id)
    {
        
        $status_data = $this->model->select_where('tbl_product','product_id',$id)->row();
        $status = $status_data->product_active_status;

        switch ($status) {
          case '1':
          $status_code = '0';
          $status_name = 'Non-Aktif';
            break;
           case '0':
          $status_code = '1';
          $status_name = 'Aktif';
            break;
        }

        $data = array(
           'product_active_status'     => $status_code
        );
        $this->model->update_data('tbl_product','product_id',$id,$data);
        $this->session->set_flashdata('data','Sukses ubah status.');
        
        $json_e  = array('RC' =>'999','ID' => $id,'STATUS_CODE' => $status_code,'STATUS_NAME' => $status_name, 'MESSAGE' => 'Sukses');
        echo json_encode($json_e);
    }
    

    

}
