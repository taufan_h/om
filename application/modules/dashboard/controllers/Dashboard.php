<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    function __construct() {
       parent::__construct();
       $this->cek_session();
     //  $this->load->model('planner/m_planner');
     //  $this->load->model('user/m_user');
    }

 function cek_session()
    {       
        if($this->session->userdata("status_login") != true)
        {
           $this->session->set_flashdata('msg_login', 'Anda harus login terlebih dahulu.');
            redirect(base_url());
        }
    }

    public function index()
    {
        $data['title'] = 'ORDERMATIX';
        $this->template->front('template/dashboard',$data);
                
    }
     public function main()
    {
        $data['title'] = 'ORDERMATIX';
        $this->template->front('template/dashboard',$data);
    }


}
