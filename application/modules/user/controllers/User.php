<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    function __construct() {
       parent::__construct();
       $this->cek_session();
       $this->load->model('m_user');
    }


 function cek_session()
    {       
        if($this->session->userdata("status_login") != true)
        {
           $this->session->set_flashdata('msg_login', 'Access Denied!, you must login.');
            redirect(base_url());
        }
    }

    function view_user($offset = null)
    {
       $sort_by = $this->input->get('sort_by');
        if(!$sort_by || $sort_by == '-') {$sort_by = 'user_fullname';}
        if($offset == null) {$offset = 0;}
        $limit = 20; 
        $data_s = array('Nama'=> 'user_fullname', 'Mitra' => 'mitra_id', 'Terakhir Login' => 'user_last_login','Status Aktif' => 'user_active_status','Nama - Abjad' => 'user_fullname');
        $client_id   = $this->session->userdata('client_id');
        $type_id    = $this->session->userdata('type_id');
        $user_id            = $this->session->userdata('user_id');        
        $department_id           = $this->session->userdata('department_id');
        
        
        $data['user_list']      = $this->m_user->select_user($client_id, $user_id, $sort_by, $offset, $limit)->result();
        $total_data = $this->m_user->total_user($client_id, $user_id)->num_rows();   
       
        $data['department'] = $this->m_user->select_data('tbl_department')->result();
        $data['user_parent'] = $this->m_user->select_user_parent()->result();
        $data['mitra'] = $this->m_user->select_where('tbl_mitra','client_id',$client_id)->result();

        $data_menu = array('menu_type_id' =>$type_id,'menu_parent_id' => '0');
        $data['menu'] = $this->m_user->select_where_array('tbl_menu',$data_menu)->result();
        $this->pagination->initialize($this->config_paging($limit, $total_data,"load"));
        $data['data_sort']      = $data_s;
        $data['title']          = 'Users - ZD';
        $data['action'] = base_url().'app/pengguna/add/proccess';
        $data['pagination'] = $this->pagination->create_links();
     
        $this->template->front('user/admin_view_user',$data);
        
    }
  
     function user_search($offset = null)
    {
        $sort_by = $this->input->post('sort_by');
        $keyword = $this->input->post('keyword');

     //   $sort_by = 'user_fullname';
     //   $keyword = 'a';

        if(!$sort_by || $sort_by == '-') {$sort_by = 'user_fullname';}
        if($offset == null) {$offset = 0;}
        $limit = 20;
        $data_s = array('Nama'=> 'user_fullname', 'Mitra' => 'mitra_id', 'Terakhir Login' => 'user_last_login','Status Aktif' => 'selisih','Nama - Abjad' => 'full_name');
        $client_id   = $this->session->userdata('client_id');
        $user_id            = $this->session->userdata('user_id');        
        $group_id           = $this->session->userdata('department_id');
        
        $data['user_list']      = $this->m_user->select_user_search($client_id, $user_id, $sort_by, $offset, $limit, $keyword)->result();
        $total_data = $this->m_user->total_user_search($client_id, $user_id, $keyword)->num_rows();   
       
      $this->pagination->initialize($this->config_paging($limit, $total_data,"search"));
        $data['sort_by']       = $sort_by;
        $data['data_sort']      = $data_s;
        $data['search']     = $keyword;
        $data['pagination'] = $this->pagination->create_links();
       $arr = array('RC' => 999, 'MESSAGE' =>  $data['user_list'], 'search' => $data['search'], 'total' => $total_data, 'paging' => $data['pagination']);
       $json = json_encode($arr);
       echo $json; 
     
        
            //$this->load->view('user/admin_view_user',$data);
        
    }
    
            
    function config_paging($limit, $total_data,$type)
    {
     
        $config['base_url']     = site_url().'app/pengguna/search/';
      
        $config['total_rows']   = $total_data;
        $config['per_page']     = $limit;
        $config["uri_segment"]  = 4;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"]    = floor($choice);
        //$config['page_query_string'] = TRUE;
        //config for bootstrap pagination class integration
        $config['attributes'] = array('class' => 'page-link');
        $config['full_tag_open']    = '<ul class="pagination">';
        $config['full_tag_close']   = '</ul>';
        $config['first_link']       = false;
        $config['last_link']        = false;
        $config['first_tag_open']   = '<li class="page-item">';
        $config['first_tag_close']  = '</li>';
        $config['prev_link']        = '&laquo';
        $config['prev_tag_open']    = '<li class="prev">';
        $config['prev_tag_close']   = '</li>';
        $config['next_link']        = '&raquo';
        $config['next_tag_open']    = '<li class="page-item">';
        $config['next_tag_close']   = '</li>';
        $config['last_tag_open']    = '<li class="page-item">';
        $config['last_tag_close']   = '</li>';
        $config['cur_tag_open']     = '<li class="page-item active"><a class="page-link" href="#">';
        $config['cur_tag_close']    = '</a></li>';
        $config['num_tag_open']     = '<li>';
        $config['num_tag_close']    = '</li>';
        
        return $config;
    }
    
    function add_proccess()
    {
        $client_id   = $this->session->userdata('client_id');
        $fullname           = $this->input->post('user_fullname');
        $username           = $this->input->post('username');
        $password           = $this->input->post('user_password');
        $mitra_id           = $this->input->post('mitra_id');
        $department_id      = $this->input->post('department_id');
        $user_parent_id     = $this->input->post('user_parent_id');
        $active_status      = $this->input->post('user_active_status');
        $user_menu          = $this->input->post('user_menu');

        $jml_user_menu = count($user_menu); 

        $replacements = array('1' => '2',
                      '2' => '3', 
                      '3' => '4',
                      '4' => '5',
                      '5' => '6',
                      'a' => 'b',
                      'b' => 'c',
                      'c' => 'd',
                     );
        $capcay             = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxysABCDEFGHIJKLMNOPQRSTUVWXYZ", 5)), 0, 5);
        $acak               = substr($capcay,0,6);
        $txt_password2      = $acak.md5($password);
        $txt_new_password   = strtr($txt_password2,$replacements);

if ($mitra_id == 1) {
  # code...
  $user_parent_id = '0';
  $type_id = '1';
}else
{

  $type_id = '2';
}
        $data = array(
            'client_id'             => $client_id,
            'user_fullname'         => $fullname,
            'username'              => $username,
            'user_password'         => $txt_new_password,
            'mitra_id'              => $mitra_id,
            'user_created_date'     => date('Y-m-d H:i:s'),
            'user_parent_id'        => $user_parent_id,
            'user_active_status'    => $active_status,
            'type_id'          => $type_id,
            'department_id'         => $department_id
        );
        $cek = $this->m_user->cek_nama('tbl_user','username',$username,$client_id)->num_rows();
        if($cek >0)
        {
        $json_e  = array('RC' =>'998','MESSAGE' => 'Username sudah ada', 'URL' => base_url().'app/pengguna');
        }else{
            if ($type_id == 1 && $jml_user_menu > 0 ) {
                # code...
                $insert = $this->m_user->insert_data('tbl_user',$data);  
                $ambil_data = $this->m_user->select_where_menu('tbl_user','username',$username)->row();
                $user_id = $ambil_data->user_id;

                for ($i=0; $i < $jml_user_menu ; $i++) { 
                  # code...
                  $datas = array('user_id' => $user_id, 'menu_id' => $user_menu[$i]);
                      $this->m_user->insert_data('tbl_user_menu',$datas);
                  $cek_child_menu = $this->m_user->select_where('tbl_menu','menu_parent_id',$user_menu[$i]);
                  if($cek_child_menu->num_rows() > 0)
                  {
                      foreach ($cek_child_menu->result() as $key) {
                        # code...
                          $datas = array('user_id' => $user_id, 'menu_id' => $key->menu_id);
                          $this->m_user->insert_data('tbl_user_menu',$datas);
                      }
                  }
                }

                $json_e  = array('RC' =>'999','MESSAGE' => 'Sukses Tambah Data', 'URL' => base_url().'app/pengguna');
            }else if ($type_id == 1 && $jml_user_menu == 0) {
              # code...
            
              $json_e  = array('RC' =>'998','MESSAGE' => 'Silakan pilih menu untuk pengguna ini', 'URL' => base_url().'app/pengguna');
            }else if ($type_id != 1) {
              # code...
                $insert = $this->m_user->insert_data('tbl_user',$data);
            
              $json_e  = array('RC' =>'998','MESSAGE' => 'Sukses Tambah Data');
            }
        }
        echo json_encode($json_e);
       
    }
    
    function user_edit($user_id)
    {
        $client_id       = $this->session->userdata('client_id');
        $department          = $this->m_user->select_data('tbl_department')->result();
        $region         = $this->m_user->select_where('tbl_region','client_id',$client_id)->result();
        $data['superior']       = $this->m_user->select_where('tbl_user','user_parent_id',$user_id)->result();
         
        $get                    = $this->m_user->ambil_data_user('tbl_user','user_id',$user_id)->row();
        $get_menu = $this->m_user->select_where('tbl_user_menu','user_id',$user_id)->result();
        $data['data']      = $get;
        $mitra = $this->m_user->select_where('tbl_mitra', 'client_id', $client_id)->result();
        $data['action']    = 'pengguna/edit/proccess';
        //$this->load->view('user/form_user',$data);
        echo json_encode(array('RC' => '999', 'DATA' => $get,'MENU' => $get_menu, 'DEPARTMENT' => $department,'MITRA' => $mitra, 'ACTION' => $data['action']));
    }
    function edit_proccess()
    {
        $client_id   = $this->session->userdata('client_id');
        $fullname           = $this->input->post('user_fullname');
        $username           = $this->input->post('username');
        $user_id           = $this->input->post('user_id');
        $password           = $this->input->post('user_password');
        $mitra_id              = $this->input->post('mitra_id');
        $department_id           = $this->input->post('department_id');
        $user_parent_id             = $this->input->post('user_parent_id');
        $active_status             = $this->input->post('user_active_status');
        $user_menu          = $this->input->post('user_menu');

        $jml_user_menu = count($user_menu); 
        $replacements = array('1' => '2',
                      '2' => '3', 
                      '3' => '4',
                      '4' => '5',
                      '5' => '6',
                      'a' => 'b',
                      'b' => 'c',
                      'c' => 'd',
                     );
        $capcay             = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxysABCDEFGHIJKLMNOPQRSTUVWXYZ", 5)), 0, 5);
        $acak               = substr($capcay,0,6);
        $txt_password2      = $acak.md5($password);
        $txt_new_password   = strtr($txt_password2,$replacements);

       if ($mitra_id == 1) {
  # code...
  $user_parent_id = '0';
  $type_id = '1';
}else
{

  $type_id = '2';
}
        
      if($password == "")
        {
           $data = array(
            'client_id'             => $client_id,
            'user_fullname'         => $fullname,
            'username'              => $username,
            'mitra_id'              => $mitra_id,
            'user_created_date'     => date('Y-m-d H:i:s'),
            'user_parent_id'        => $user_parent_id,
            'user_active_status'    => $active_status,
            'type_id'          => $type_id,
            'department_id'         => $department_id
        );
        }else
        {
            $data = array(
            'client_id'             => $client_id,
            'user_fullname'         => $fullname,
            'username'              => $username,
            'user_password'         => $txt_new_password,
            'mitra_id'              => $mitra_id,
            'user_created_date'     => date('Y-m-d H:i:s'),
            'user_parent_id'        => $user_parent_id,
            'type_id'          => $type_id,
            'user_active_status'    => $active_status,
            'department_id'         => $department_id
        );
        }

        
      
        $cek = $this->m_user->cek_nama_not_in('tbl_user','username','user_id',$username,$client_id,$user_id)->num_rows();
        if($cek >0)
        {
        $json_e  = array('RC' =>'998','MESSAGE' => 'Username sudah ada', 'URL' => base_url().'app/pengguna');
        }else{
          if ($jml_user_menu > 0 ) {
                # code...
                $this->m_user->update_data('tbl_user','user_id',$user_id,$data);
               
                  $this->m_user->delete_data('tbl_user_menu','user_id',$user_id);
                for ($i=0; $i < $jml_user_menu ; $i++) { 
                  # code...
                  $datas = array('user_id' => $user_id, 'menu_id' => $user_menu[$i]);
                      $this->m_user->insert_data('tbl_user_menu',$datas);
                   $cek_child_menu = $this->m_user->select_where('tbl_menu','menu_parent_id',$user_menu[$i]);
                  if($cek_child_menu->num_rows() > 0)
                  {
                      foreach ($cek_child_menu->result() as $key) {
                        # code...
                          $datas = array('user_id' => $user_id, 'menu_id' => $key->menu_id);
                          $this->m_user->insert_data('tbl_user_menu',$datas);
                      }
                  }
                }
              $json_e  = array('RC' =>'999','MESSAGE' => 'Data Berhasil diedit', 'URL' => base_url().'app/pengguna');
            }else
            {
              $json_e  = array('RC' =>'998','MESSAGE' => 'Silakan pilih menu untuk pengguna ini', 'URL' => base_url().'app/pengguna');
            }

            
      
            
        }
        echo json_encode($json_e);
    }
    function change_status($user_id)
    {
        $client_id   = $this->session->userdata('client_id');
        
        $status_data = $this->m_user->select_where('tbl_user','user_id',$user_id)->row();
        $status = $status_data->user_active_status;

        switch ($status) {
          case '1':
          $status_code = '0';
          $status_name = 'Non-Aktif';
            break;
           case '0':
          $status_code = '1';
          $status_name = 'Aktif';
            break;
        }

        $data = array(
           'user_active_status'     => $status_code
        );
        $this->m_user->update_data('tbl_user','user_id',$user_id,$data);
        $this->session->set_flashdata('data','Sukses ubah status.');
        
        $json_e  = array('RC' =>'999','USER_ID' => $user_id,'STATUS_CODE' => $status_code,'STATUS_NAME' => $status_name, 'MESSAGE' => 'Sukses');
        echo json_encode($json_e);
    }
    
    function xls_user()
    {
      $tbl='User List<br><br><table border="1" width="100%" style="text-align:center;">';
      $tbl.='<tr height="30" bgcolor="#0C82E5">
        <th id="th1">No.</th>
        <th id="th1">Full Name</th>
        <th id="th1">Username</th>
        <th id="th1">Superior</th>
        <th id="th1">Status</th>
        <th id="th1">Region</th>
        <th id="th1">Created Date</th>
        <th id="th1">Last Login</th>
        <th id="th1">Aging</th>
        </tr>';           
      
      $client_id   = $this->session->userdata('client_id');
        $user_id            = $this->session->userdata('user_id');        
        $group_id           = $this->session->userdata('group_id');
        
     if($group_id == '1')
     {
          $data      = $this->m_user->xls_select_user($client_id, $user_id, 'username');
       
     }else
     {
          $data      = $this->m_user->xls_select_user_manager($client_id, $user_id, 'username'); 
       
     }
      $region         = $this->m_user->select_where('tbl_region','client_id',$client_id)->result();
        
      $parent_user       = $this->m_user->select_where('tbl_user','client_id',$client_id)->result();
       if ($data->num_rows() > 0) {
         $no = 1;
          foreach($data->result() as $value)
          {
             $region_name = "-";
             foreach ($region as $v_region) {
                if($value->region_id == $v_region->region_id)
                {
                     $region_name = $v_region->region_name;
                }
            }
            $superior = "-";
            foreach($parent_user as $v_user) {
                if($value->parent_user_id == $v_user->user_id)
                {
                     $superior = $v_user->username;
                }
            }
            if($value->active_status == 1) {$status = 'Active';}else {$status = 'Disabled';}
                                                
            if($value->active_status == '1'){$enable = "<i class='ti-check'>"; $stt = '0';}else {$enable = "<i class='ti-close'>"; $stt = '1';}
           
            $tbl.="<tr height='25'>
                  <td align='center'>".$no."</td>   
                  <td>".$value->full_name."</td>
                  <td>".$value->username."</td>
                  <td>".$superior."</td>
                  <td>".$status."</td>
                  <td>Sales ".$region_name."</td>
                  <td>".$value->user_date_in." (GMT+7)</td>
                  <td>".$value->user_last_login." (GMT+7)</td>
                  <td>".$value->selisih." Day's Inactive</td>
                 </tr>";
            $no++;
          }
       }else
       {

      $tbl .= '<tr><td colspan="8">No data</td></tr>';
       }
      

      
      $tbl .='</table><br><br><i>Created & downloaded from ZD</i>';        
      $namafile = "Users xls";
      header("Content-type: application/vnd.ms-excel");
      header("Content-Disposition: attachment; filename=\"$namafile.xls\"");
      echo $tbl;
    }
    
    function select_region()
    {
        $superior = $this->input->post('superior');
        $client_id   = $this->session->userdata('client_id');       
        $group_id           = $this->session->userdata('group_id');
         $arr = array();
        if($superior == '0')
        {
            $parent_region     = $this->m_user->select_where('tbl_region','client_id',$client_id)->result();
                foreach ($parent_region as $value) {
                $data['REGION_ID'] = $value->region_id;
                $data['REGION_NAME'] = $value->region_name;

                array_push($arr, $data);
            }
        }else
        {
            $region_superior = $this->m_user->select_where('tbl_user','user_id',$superior)->row();
         $idnya = $region_superior->region_id;
        
       $region_id         = $this->m_user->select_where('tbl_region','region_id',$idnya)->result();
       $parent_region       = $this->m_user->select_where('tbl_region','parent_region_id',$idnya)->result();
       foreach ($region_id as $value) {
           $data['REGION_ID'] = $value->region_id;
           $data['REGION_NAME'] = $value->region_name;
           
           array_push($arr, $data);
       }
       foreach ($parent_region as $value_1) {
           $dat['REGION_ID'] = $value_1->region_id;
           $dat['REGION_NAME'] = $value_1->region_name;
           
           array_push($arr, $dat);
       }
        }
    $json_e  = array('RC' =>'999','MESSAGE' => $arr);
        echo json_encode($json_e);
    }

}
