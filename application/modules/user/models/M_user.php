<?php

class M_user extends CI_Model {
    //put your code here
    public $mitra_id;
    public $client_id;
    public function __construct() {
        parent::__construct();
        $this->mitra_id = $this->session->userdata('client_id');
        $this->client_id = $this->session->userdata('client_id');
    }
    
    function select_user($client_id, $user_id, $sort_by, $offset, $limit)
    {
        $result = $this->db->query("select *, datediff(now(),user_last_login) as selisih FROM tbl_user  
                    inner join tbl_department using (department_id)   
                    inner join tbl_mitra using (mitra_id)    
                    WHERE tbl_user.client_id = '".$client_id."' AND 
                    user_id <> '".$user_id."' ORDER BY ".$sort_by." asc LIMIT $offset, $limit");
        return $result;
    }
    function select_user_parent()
    {
        $result = $this->db->query("select * from tbl_user
        inner join tbl_department using (department_id) 
                    inner join tbl_mitra using (mitra_id)    
         where tbl_user.client_id = '".$this->client_id."' && mitra_id = '".$this->mitra_id."' && user_parent_id = '0'");
        return $result;
    }
    function ambil_data_user($tbl_user,$field,$user_id)
    {
        $result = $this->db->query("select * FROM tbl_user  
                    inner join tbl_department using (department_id)    
                    inner join tbl_mitra using (mitra_id)   
                    WHERE user_id = '".$user_id."'");
        return $result;
    }
    function select_data($table)
    {
        $this->db->select('*');
        return $this->db->get($table);
    }
    
    function total_user($client_id, $user_id)
    {
        $result = $this->db->query("select *, datediff(now(),user_last_login) as selisih FROM tbl_user 
                    inner join tbl_department using (department_id)  
                    WHERE tbl_user.client_id = '".$client_id."'");
        return $result;
    }
    
    function cek_nama($table, $field, $record, $client_id)
    {
        $this->db->select('*')
                ->where($field,$record)
                ->where('client_id',$client_id);
                
        $sql = $this->db->get($table);
        return $sql;
                
    }
     function cek_nama_not_in($table, $field_where,$field_not_id, $record, $client_id,$user_id)
    {
        $this->db->select('*')
                ->where($field_where,$record)
                ->where_not_in($field_not_id,$user_id)
                ->where('client_id',$client_id);
                
        $sql = $this->db->get($table);
        return $sql;
                
    }
    function select_where($table, $field, $record)
    {
        $this->db->select('*')
                ->where($field,$record)
                ->from($table);
        return $this->db->get();
               
    }
      function select_where_array($table, $array)
    {
        $this->db->select('*')
                ->where($array)
                ->from($table);
        return $this->db->get();
               
    }
    function delete_data($table, $field, $record)
    {
        $this->db->where($field, $record)
        ->delete($table);
    }
    function select_where_menu($table, $field, $record)
    {
        $this->db->select('*')
                ->where($field,$record)
                ->where('client_id',$this->client_id)
                ->from($table);
        return $this->db->get();
               
    }
    function insert_data($table,$data)
    {
        return $this->db->insert($table, $data);
    }
    function update_data($table,$field,$record,$data)
    {
        $this->db->where($field, $record);
        $sql = $this->db->update($table, $data); 
        return $sql;
    }

     function select_user_search($client_id, $user_id, $sort_by, $offset, $limit, $keyword)
    {
        $result = $this->db->query("select * FROM tbl_user 

                    inner join tbl_department using (department_id)  
                    inner join tbl_mitra using (mitra_id)    
            WHERE tbl_user.client_id = '".$client_id."' AND "
                . "user_id <> '".$user_id."' && user_fullname like '%".$keyword."%' or username like '%".$keyword."%' ORDER BY ".$sort_by." asc LIMIT $offset, $limit");
        return $result;
    }
   
    function total_user_search($client_id, $user_id, $keyword)
    {
        $result = $this->db->query("select *, datediff(now(),"
                . "user_last_login) as selisih FROM tbl_user  "
                . "
                    inner join tbl_department using (department_id) 
                    inner join tbl_mitra using (mitra_id)      WHERE tbl_user.client_id = '".$client_id."' AND "
                . "user_id <> '".$user_id."' && user_fullname like '%".$keyword."%' or username like '%".$keyword."%'");
        return $result;
    }
   
    
}
