 <div class="ks-column ks-page">
        <div class="ks-header">
            <section class="ks-title">
                <h3>Pengguna</h3>

                <div class="ks-controls">
                    <button class="btn btn-primary-outline ks-light ks-filemanager-navigation-block-toggle" data-block-toggle=".ks-filemanager-page > .ks-navigation">Navigation</button>
                    <button class="btn btn-primary-outline ks-light ks-filemanager-info-block-toggle" data-block-toggle=".ks-filemanager-page > .ks-info">Info</button>
                </div>
            </section>
           <!-- <center><div class="message"><span class="badge badge-pill badge-success"><div class="alert" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Warning!</strong> Better check yourself, you're not looking too good.
</div></span></center>-->
        </div>

        <div class="ks-content">
            <div class="ks-body ks-filemanager-page">
                <div class="ks-navigation ks-scrollable" data-auto-height>
    <div class="ks-wrapper">
        <div class="ks-separator tambah_baru">Tambah Baru</div>
            <ul class="ks-tree">
                <li class="ks-has-submenu">
                <a>
                    <form id="F1">
                        <div class="form-group">
                            <label for="subject">User ini untuk Mitra</label><br/>
                            <select name="mitra_id" onchange="showAtasan()" class="form-control ks-select mitra_id" id="select2-id-label-single mitra_id">
                             <?php
                                foreach ($mitra as $key) {
                                    # code...
                                    echo "<option value='".$key->mitra_id."'>".$key->mitra_name."</option>";
                                }
                            ?>
                            </select>
                        </div>                      
                        <div class="form-group">
                            <label for="subject">Nama Lengkap</label>
                            <input type="text" name="user_fullname" class="form-control" id="user_fullname" placeholder="Nama Lengkap">
                            <input type="hidden" name="user_id" class="form-control" id="user_id">
                            <input type="hidden" name="action" class="form-control" id="action" value="<?php echo $action; ?>">
                        </div>
                        <div class="form-group">
                            <label for="subject">Nama User</label>
                            <input type="text" name="username" class="form-control" id="username" placeholder="Nama User/Username">
                        </div>
                        <div class="form-group">
                            <label for="subject">Password</label>
                            <input type="password" name="user_password" class="form-control" id="user_password" placeholder="Password">
                        </div> 
                        <div class="form-group" id="atasan">
                            <label for="subject">Atasan</label>
                            <select name="user_parent_id" class="form-control ks-select" id="select2-id-label-single user_parent_id">
                               <?php
                                foreach ($user_parent as $key) {
                                    # code...
                                    echo "<option value='".$key->user_id."'>".$key->user_fullname." - ".$key->department_name."</option>";
                                }
                            ?>
                            </select>
                        </div>                                           
                        <div class="form-group">
                            <label for="subject">department</label><br/>
                            <select name="department_id" class="form-control ks-select department_id" id="select2-id-label-single department_id">
                            <?php
                                foreach ($department as $key) {
                                    # code...
                                    echo "<option value='".$key->department_id."'>".$key->department_name."</option>";
                                }
                            ?>
                            </select>
                        </div> 
                         <div class="form-group">
                            <label for="subject">Status Login</label><br/>
                            <select name="user_active_status" class="form-control ks-select user_active_status" id="select2-id-label-single user_active_status">
                                <option value="1">Aktif</option>
                                <option value="0">Tidak Aktif</option>
                            </select>
                        </div> 
                        <div class="form-group" id="menu_akses">
                            <label for="subject">Akses Menu</label><br/>
                            <?php
                                foreach ($menu as $key) {
                                    # code...
                                    echo '<input type="checkbox" name="user_menu[]" value="'.$key->menu_id.'" class="ks-select user_menu" id="user_menu_'.$key->menu_id.'">'.$key->menu_name.'</input><br>';
                                }
                            ?>
                        </div>                    
                        <div class="form-group">
                            <div class="ks-actions">
                                <button type="button" onclick="saveData()" class="btn btn-success">Kirim</button>
                                <input type="reset" class="btn btn-primary-outline ks-light ks-view-cancel" value="Batal"></input>
                                </button>
                            </div>
                        </div>
                    </form>
                </a>
            </li>
        </ul>
    </div>
</div>

<div class="ks-files"> 

    <div class="ks-header">

        <div class="ks-search input-icon icon-right icon icon-lg">
            <input id="input-group-icon-text" type="text" class="form-control cari" id="cari" placeholder="Cari...">
            <span class="icon-addon">
                <span class="fa fa-search" onclick="setData()"></span>
            </span>
        </div>
        <div class="ks-filters">
            <div class="ks-sortable btn-group">
                <button class="btn btn-primary-outline ks-light dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="btn-text-lighter">Urutkan</span>
                </button>
                <div class="dropdown-menu dropdown-menu-right ks-dropdown-menu-sortable urutkan">
                        <input name="sort_by" id="sort_by" type="hidden" value="user_fullname">
                    <a class="dropdown-item dropdown-item-checked urutkan-dropdown" id="user_fullname" onclick="urutkan('user_fullname')" href="#">
                        <span class="ks-sort-by-text">Berdasarkan</span> Nama
                        <input name="sort_name" id="user_fullname" type="radio">
                    </a>
                    <a class="dropdown-item urutkan-dropdown" href="#" onclick="urutkan('mitra_name')" id="sort_mitra">
                        <span class="ks-sort-by-text">Berdasarkan</span> Mitra
                        <input name="sort_mitra" id="mitra_name" type="radio">
                    </a>
                    <a class="dropdown-item urutkan-dropdown" href="#" onclick="urutkan('user_last_login')" id="sort_last_login">
                        <span class="ks-sort-by-text">Berdasarkan</span> Terakhir Login
                        <input name="sort_last_login" id="user_last_login" type="radio">
                    </a>
                    <a class="dropdown-item urutkan-dropdown" href="#" onclick="urutkan('user_active_status')" id="sort_status">
                        <span class="ks-sort-by-text">Berdasarkan</span> Status Aktif
                        <input name="sort_status" id="user_active_status" type="radio">
                    </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item urutkan-dropdown" href="#" onclick="urutkan('user_fullname')" id="sort_abjad_name">
                        <span>Berdasarkan Nama - Abjad</span>
                        <input name="sort_abjad_name" id="user_fullname" type="radio">
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="ks-content ks-scrollable" data-auto-height data-fix-height="40">
        <!--table-->
        <div class="card-block">
            <table class="table table-bordered vertical-align-middle tabelnya" id="tabelnya">
                <thead>
                <tr>
                    <th width="160">Nama Mitra</th>
                    <th width="160">Nama Lengkap</th>
                    <th width="80">Nama User</th>
                    <th width="50">Status Aktif</th>
                    <th width="80">Dibuat</th>
                    <th width="80">Terakhir Login</th>
                    <th>&nbsp;</th>
                </tr>
                </thead>
                <tbody id="tbody">
                <?php
                foreach ($user_list as $key) {
                    if ($key->selisih == '0') {
                        # code...
                        $selisih = "Hari ini";
                    }else
                    {
                        $selisih = $key->selisih." Hari lalu";
                    }

                    if ($key->user_last_login != "0000-00-00 00:00:00") {
                        # code...
                        $last_login = date("d-m-Y", strtotime($key->user_last_login));
                    }else
                    {
                        $last_login = "-";  
                        $selisih = "- Hari lalu";
                    }
                    switch ($key->user_active_status) {
                        case '1':
                            # code...
                        $status = '<span class="badge badge-pill badge-success">Aktif</span>';
                        $status1 = "Non-Aktif";
                        $status_balik = "0";
                            break;
                        case '0':
                            # code...
                        $status = '<span class="badge badge-pill badge-danger">Non-Aktif</span>';
                        $status1 = "Aktif";
                        $status_balik = "1";
                            break;
                    }
                    # code...
                    echo '
                        <tr class="row-checked" id="tr_'.$key->user_id.'">
                    <td>
                        <div class="text-block">
                            <div class="text-block-container">
                                <div class="text-block-text">'.$key->mitra_name.'</div>
                                <div class="text-block-sub-text">'.$key->mitra_city.'</div>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="table-cell-block image">
                            <div class="image-block-container">
                                <img src="'.includes_url.'uploads/avatar/'.$key->user_photo.'" class="rounded-circle" width="36" height="36">
                            </div>
                            <div class="text-block-container">
                                <div class="text-block-text">'.$key->user_fullname.'</div>
                                <div class="text-block-sub-text">'.$key->department_name.'</div>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="table-cell-block">
                            <div class="text-block-container">
                                <div class="text-block-text">'.$key->username.'</div>
                            </div>
                        </div>
                    </td>                    
                    <td>
                        <div class="table-cell-block status">
                            <div class="status-block-container">
                                <span class="user_active_status_'.$key->user_id.'">'.$status.'</span>
                            </div>
                        </div>
                    </td>                                        
                    <td>
                        <div class="table-cell-block">
                            <div class="text-block-container">
                                <div class="text-block-text">'.date("d-m-Y", strtotime($key->user_created_date)).'</div>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="table-cell-block">
                            <div class="text-block-container">
                                <div class="text-block-text">'.$last_login.'</div>
                                <div class="text-block-sub-text">'.$selisih.'</div>
                            </div>
                        </div>
                    </td>                    
                    <td class="table-actions">
                        <div class="dropdown">
                            <a class="btn btn-link" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="fa fa-ellipsis-h"></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                                <a class="dropdown-item" href="#" onClick=edit("'.base_url().'app/pengguna/edit/'.$key->user_id.'")>
                                    <span class="fa fa-pencil icon text-primary-on-hover"></span> Edit
                                </a>
                                <a class="dropdown-item" href="#" onClick=changeStatus("'.base_url().'app/pengguna/change_status/'.$key->user_id.'")><span class="fa fa-remove icon text-danger-on-hover "></span><span class="aksi_user_active_status_'.$key->user_id.'">'.$status1.'</span></a>
                            </div>
                        </div>
                    </td>
                </tr>
                    ';
                }
                ?>               
                </tbody>
            </table>

            <!--paging-->
            <div class="ks-items-block pages_paging" id="pages_paging"><nav>
                <?php echo $pagination; ?></nav>
            </div>
            <!--end paging-->
        </div>
        <!--end--> 
    </div>
</div>
<script type="text/javascript">



function changeStatus(url)
{
    $.ajax({
    type: "GET",
    url: url,
    cache: false,                  
    success:function(response) {
        console.log(response);
        var objJSON = JSON.parse(response);
        var user_id = objJSON.USER_ID;
        var status_name = objJSON.STATUS_NAME;
        if (objJSON.STATUS_CODE == '1') {
            var revert_status_name = 'Non-Aktif';
        }else
        {
            var revert_status_name = 'Aktif';
        }
        $('.aksi_user_active_status_'+user_id).html(revert_status_name);
        if (objJSON.STATUS_CODE == '1') {
            $('.user_active_status_'+user_id).html('<span class="badge badge-pill badge-success">Aktif</span>');
        }else 
        {
            $('.user_active_status_'+user_id).html('<span class="badge badge-pill badge-danger">Non-Aktif</span>');
        }
    }
});
}
// fungsi tambah baru
function saveData()
{
    var url = $('#action').val();

        $.ajax({
    type: "post",
    url: url,
    cache: false,               
    data: $('#F1').serialize(),    
    success:function(response) {
      //console.log(response); 
      var objJSON = JSON.parse(response); 
      if (objJSON.RC != 999) {
            alert(objJSON.MESSAGE);
      }else 
      {
        //console.log(objJSON.RC);
           // alert("Tambah data sukses");
           alert(objJSON.MESSAGE);
        setData();
        bersihkanForm();
       // setPaging();
      }    
    
    }
}); 
}
function showAtasan()
{
    var mitra_id = $('.mitra_id option:selected').val();

     if(mitra_id != 1)
     {
        $('#atasan').css({display: "none"});
        $('#menu_akses').css({display: "none"});
    }else 
    {
        $('#atasan').css({display: "inline"});
        $('#menu_akses').css({display: "inline"});
    }
}
function bersihkanForm()
{
    $(".tambah_baru").html('Tambah Data');
    $('#user_password').val('');
    $('#username').val('');
    $('#user_fullname').val('');
   $('input:checkbox').removeAttr('checked');
}

// untuk menampilkan data ke tabel
function setData()
{
    var cari = $('.cari').val();
    var sort_by = $('#sort_by').val();
     $.ajax({
        type: "post",
        url: '<?php echo base_url()."app/pengguna/search";?>',
        data: {sort_by : sort_by, keyword : cari},
        success: function(res){
                       console.log(res);
            var obj = JSON.parse(res);
        if (obj.RC == 999) {
           setContent(obj);
        }
        
        }
         
    });
 

}


function urutkan(sort_by)
{
    $('#sort_by').val(sort_by);
    $('.urutkan a').removeClass('dropdown-item-checked');
    var sort =  $('#sort_by').val();
    setData();
}
function edit(url)
{
   $(".tambah_baru").html('Edit Data');
 
     $.ajax({
    type: "GET",
    url: url,
    cache: false,                  
    success:function(response) {
        //console.log(response);
        var objJSON = JSON.parse(response);
        var user_id = objJSON.DATA.user_id;
        var action = objJSON.ACTION;
        var username = objJSON.DATA.username;
        var user_fullname = objJSON.DATA.user_fullname;
        var active_status = objJSON.DATA.user_active_status;
        var old_mitra_id = objJSON.DATA.mitra_id;
        var old_mitra_name = objJSON.DATA.mitra_name;
        var department_id = objJSON.DATA.department_id;
        var department_name = objJSON.DATA.department_name;
        $('#user_fullname').val(user_fullname);
        $('#username').val(username);        
        $('#user_id').val(user_id);       
        $('#action').val(action);
        $('#user_password').val('');

        var list_mitra = objJSON.MITRA;
        var list_department = objJSON.DEPARTMENT;
        var list_menu = objJSON.MENU;
        
        $('.user_active_status').empty(); 
        $('.mitra_id').empty();
        $('.department_id').empty();

        if (objJSON.DATA.user_active_status != 0) {            
            $('.user_active_status').append("<option value='1'>Aktif</option>"); 
            $('.user_active_status').append("<option value='0'>Tidak Aktif</option>");
        }else 
        {
            $('.user_active_status').append("<option value='0'>Tidak Aktif</option>");
            $('.user_active_status').append("<option value='1'>Aktif</option>"); 
        } 

        $('.mitra_id').append("<option value='"+old_mitra_id+"'>"+old_mitra_name+"</option>");
           $.each(list_mitra, function(key, value) {
           if (value.mitra_id != old_mitra_id) {            
            $('.mitra_id').append("<option value='"+value.mitra_id+"'>"+value.mitra_name+"</option>"); 
           }                  
        });

        $('.department_id').append("<option value='"+department_id+"'>"+department_name+"</option>");
           $.each(list_department, function(key, value) {
           if (value.department_id != department_id) {            
            $('.department_id').append("<option value='"+value.department_id+"'>"+value.department_name+"</option>"); 
           }                  
        });
 $.each(list_menu, function(key, value) {

           //if (value.menu_id == $('user_menu_'+value.menu_id).val()) {            
            $('#user_menu_'+value.menu_id).attr('checked','checked')
           //}                  
        });
        
    }
});

}
function setContent(obj)
{
    var content = '';
           for (i = 0; i < obj.MESSAGE.length; i++) {
            if(obj.MESSAGE[i].user_active_status == '1') {
                var status = '<span class="badge badge-pill badge-success">Aktif</span>'; var status1 = 'Non-Aktif';}else {var status = '<span class="badge badge-pill badge-danger">Non-Aktif</span>';
            var status1 = 'Aktif'; }

                content += '<tr class="row-checked" id="tr_'+obj.MESSAGE[i].user_id+'">';
                content += ' <td><div class="text-block"><div class="text-block-container"><div class="text-block-text">'+obj.MESSAGE[i].mitra_name+'</div><div class="text-block-sub-text">'+obj.MESSAGE[i].mitra_city+'</div></div></div></td>';
                content += '<td><div class="table-cell-block image"><div class="image-block-container"> <img src="<?php echo includes_url;?>uploads/avatar/'+obj.MESSAGE[i].user_photo+'" class="rounded-circle" width="36" height="36"> </div><div class="text-block-container"><div class="text-block-text">'+obj.MESSAGE[i].user_fullname+'</div><div class="text-block-sub-text">'+obj.MESSAGE[i].department_name+'</div></div></div></td>';
                content += '<td><div class="table-cell-block"><div class="text-block-container"><div class="text-block-text">'+obj.MESSAGE[i].username+'</div></div></div></td>';
                content += '<td><div class="table-cell-block status"><div class="status-block-container"><span class="user_active_status_'+obj.MESSAGE[i].user_id+'">'+status+'</span></div></div></td> ';
                content += '<td><div class="table-cell-block"><div class="text-block-container"><div class="text-block-text">'+obj.MESSAGE[i].user_created_date+'</div></div></div></td>';
                content += '<td><div class="table-cell-block"><div class="text-block-container"><div class="text-block-text">'+obj.MESSAGE[i].user_last_login+'</div></div></div></td>';
                content += '<td class="table-actions"><div class="dropdown"><a class="btn btn-link" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="fa fa-ellipsis-h"></span></a><div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1"><a class="dropdown-item" href="#" onClick=edit("<?php echo base_url();?>app/pengguna/edit/'+obj.MESSAGE[i].user_id+'")><span class="fa fa-pencil icon text-primary-on-hover"></span> Edit</a><a class="dropdown-item" href="#" onClick=changeStatus("<?php echo base_url();?>app/pengguna/change_status/'+obj.MESSAGE[i].user_id+'")><span class="fa fa-remove icon text-danger-on-hover "></span><span class="aksi_user_active_status_'+obj.MESSAGE[i].user_id+'">'+status1+'</span></a></div></div> </td>';
                content += '</tr>';
                            
            }
            //console.log(content);
            $("#tabelnya tbody").html(content);
            $("#pages_paging").html(obj.paging);
}
</script>
