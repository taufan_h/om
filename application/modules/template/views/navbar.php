<?php
$type_id = $this->session->userdata('type_id');
$user_id = $this->session->userdata('user_id');

if ($type_id == '1') {
    # code...
        $menu = $this->m_navbar->select_menu_1($user_id)->result();
}else
{

    $menu = $this->m_navbar->select_menu()->result();
}

?>
    <!-- BEGIN HEADER -->
<nav class="navbar ks-navbar">
    <!-- BEGIN HEADER INNER -->
    <!-- BEGIN LOGO -->
    <div href="index.html" class="navbar-brand">
        <!-- BEGIN RESPONSIVE SIDEBAR TOGGLER -->
        <a href="#" class="ks-sidebar-toggle"><i class="fa fa-bars" aria-hidden="true"></i></a>
        <a href="#" class="ks-sidebar-mobile-toggle"><i class="fa fa-bars" aria-hidden="true"></i></a>
        <!-- END RESPONSIVE SIDEBAR TOGGLER -->
        <a href="dashboard.php" class="ks-logo"><img src="<?php echo assets_url; ?>img/ico/favicon.png" width="50"> ORDERMATIX</a>
    </div>
    <!-- END LOGO -->

    <!-- BEGIN MENUS -->
    <div class="ks-wrapper">
        <nav class="nav navbar-nav">
            <!-- BEGIN NAVBAR MENU -->
            <div class="ks-navbar-menu">
           
            </div>
            <!-- END NAVBAR MENU -->

            <!-- BEGIN NAVBAR ACTIONS -->
            <div class="ks-navbar-actions">
                <!-- BEGIN NAVBAR LANGUAGES -->
               <!-- <div class="nav-item dropdown ks-languages">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                        Bahasa <span class="ks-text">Bahasa</span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="Preview">
                        <div class="ks-wrapper">
                            <a href="#" class="ks-language">
                                <span class="flag-icon flag-icon-us ks-icon"></span>
                                <span class="ks-text">English</span>
                            </a>
                            <a href="#" class="ks-language">
                                <span class="flag-icon flag-icon-id ks-icon"></span>
                                <span class="ks-text">Indonesia</span>
                            </a>
                        </div>
                    </div>
                </div>-->
                <!-- END NAVBAR LANGUAGES -->

             
                <!-- BEGIN NAVBAR USER -->
                <div class="nav-item dropdown ks-user">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                        <span class="ks-avatar">
                            <img id="ava" src="<?php echo base_url()."includes/uploads/avatar/".$this->session->userdata('user_photo'); ?>" width="36" height="36">
                        </span>
                        <span class="ks-info">
                            <span class="ks-name">Steve Vai</span>
                            <span class="ks-description">Delivery</span>
                        </span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="Preview">
                        <a class="dropdown-item" href="<?php echo base_url()."app/profil";?>">
                            <span class="fa fa-user-circle-o ks-icon"></span>
                            <span>Profil</span>
                        </a>
                       <!-- <a class="dropdown-item" href="#">
                            <span class="fa fa-question-circle ks-icon" aria-hidden="true"></span>
                            <span>Bantuan</span>
                        </a>-->
                        <a class="dropdown-item" href="<?php echo base_url().'app/logout';?>">
                            <span class="fa fa-sign-out ks-icon" aria-hidden="true"></span>
                            <span>Keluar</span>
                        </a>
                    </div>
                </div>
                <!-- END NAVBAR USER -->
            </div>
            <!-- END NAVBAR ACTIONS -->
        </nav>

        <!-- BEGIN NAVBAR ACTIONS TOGGLER -->
        <nav class="nav navbar-nav ks-navbar-actions-toggle">
            <a class="nav-item nav-link" href="#">
                <span class="fa fa-ellipsis-h ks-icon ks-open"></span>
                <span class="fa fa-close ks-icon ks-close"></span>
            </a>
        </nav>
        <!-- END NAVBAR ACTIONS TOGGLER -->

        <!-- BEGIN NAVBAR MENU TOGGLER -->
        <nav class="nav navbar-nav ks-navbar-menu-toggle">
            <a class="nav-item nav-link" href="#">
                <span class="fa fa-th ks-icon ks-open"></span>
                <span class="fa fa-close ks-icon ks-close"></span>
            </a>
        </nav>
        <!-- END NAVBAR MENU TOGGLER -->
    </div>
    <!-- END MENUS -->
    <!-- END HEADER INNER -->
</nav>
<!-- END HEADER -->

<!-- BEGIN NAVBAR HORIZONTAL -->
<div class="ks-navbar-horizontal ks-info">
    <ul class="nav nav-pills">
        <?php
        foreach ($menu as $key) {
            # code...
            if ($key->menu_parent_id == '0') {
                # code...
                if ($key->menu_have_child == '0') {
                    # code...
                    echo '
                    <li class="nav-item">
                        <a class="nav-link" href="'.base_url().$key->menu_url.'">
                            <span class="ks-text">'.$key->menu_name.'</span>
                        </a>
                    </li>  
                ';
                }else
                {
                    echo '
                        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                '.$key->menu_name.'
            </a>
            <div class="dropdown-menu">';
            foreach ($menu as $key2) {
                # code...
                if ($key2->menu_parent_id == $key->menu_id) {
                    # code...
                    echo '
                <a class="dropdown-item" href="'.$key2->menu_url.'">'.$key2->menu_name.'</a>';
                }
            }
                
             echo' </div>
                    </li>
                    ';
                }
            }
        }
        ?>      
       
    </ul>
</div>
<!-- END NAVBAR HORIZONTAL -->
<div class="ks-container" id="halaman">