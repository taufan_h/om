</div>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo libs_url; ?>jquery/jquery.min.js"></script>
<script src="<?php echo libs_url; ?>responsejs/response.min.js"></script>
<script src="<?php echo libs_url; ?>loading-overlay/loadingoverlay.min.js"></script>
<script src="<?php echo libs_url; ?>tether/js/tether.min.js"></script>
<script src="<?php echo libs_url; ?>bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo libs_url; ?>jscrollpane/jquery.jscrollpane.min.js"></script>
<script src="<?php echo libs_url; ?>jscrollpane/jquery.mousewheel.js"></script>
<script src="<?php echo libs_url; ?>flexibility/flexibility.js"></script>
<script src="<?php echo libs_url; ?>select2/js/select2.min.js"></script>

<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="<?php echo assets_url; ?>scripts/common.min.js"></script>
<!-- END THEME LAYOUT SCRIPTS -->

<script src="<?php echo libs_url; ?>momentjs/moment-with-locales.min.js"></script>
<script src="<?php echo libs_url; ?>bootstrap-daterange-picker/daterangepicker.js"></script>

<script type="application/javascript">

(function ($) {
    $(document).ready(function() {
        var ksNavigationTree = $('.ks-filemanager-page .ks-navigation .ks-tree');
        var ksNavigationMenu = $('.ks-filemanager-page .ks-navigation .ks-menu');
        var ksContentItemsCheckboxes = $('.ks-filemanager-page .ks-content .ks-item :checkbox');
        var ksInfo = $('.ks-filemanager-page > .ks-info');
        var ksInfoCancel = $('.ks-filemanager-page > .ks-info .ks-info-cancel');

        ksNavigationTree.on('click', 'li > a', function() {
            var $parent = $(this).parent();
            var isNested = $parent.parents('.ks-submenu').size();

            ksNavigationMenu.find('li.ks-active').removeClass('ks-active');

            if (!isNested) {
                if ($parent.hasClass('ks-open')) {
                    $parent.removeClass('ks-open');
                } else {
                    ksNavigationTree.find('.ks-open').removeClass('ks-open');
                    $parent.addClass('ks-open');
                }
            } else {
                if ($parent.hasClass('ks-open')) {
                    $parent.removeClass('ks-open');
                } else {
                    if (!$parent.hasClass('ks-has-submenu')) {
                        $parent.addClass('ks-active');
                    }

                    $parent.addClass('ks-open');
                }
            }
        });

        ksNavigationMenu.on('click', 'li > a', function() {
            var $parent = $(this).parent();

            ksNavigationTree.find('li.ks-active').removeClass('ks-active');

            if ($parent.hasClass('ks-active')) {
                $parent.removeClass('ks-active');
            } else {
                ksNavigationMenu.find('li.ks-active').removeClass('ks-active');
                $parent.addClass('ks-active');
            }
        });

        ksContentItemsCheckboxes.on('click', function() {
            var $parent = $(this).closest('.ks-item');

            if ($parent.hasClass('ks-checked')) {
                $parent.removeClass('ks-checked');
            } else {
                $parent.addClass('ks-checked');
            }
        });

        /*$('.fmb-list-content td').on('click', function() {
         var $parent = $(this).closest('tr');

         if ($parent.hasClass('ks-selected')) {
         $parent.removeClass('ks-selected');
         } else {
         $('.fmb-list-content tr.selected').removeClass('ks-selected');
         $parent.addClass('ks-selected');
         }
         });

         $('.fmb-list-content .checkbox-column :checkbox').on('click', function(e) {
         var $parent = $(this).closest('tr');

         if ($parent.hasClass('ks-checked')) {
         $parent.removeClass('ks-checked');
         } else {
         $parent.addClass('ks-checked');
         }
         });*/

        $('.ks-dropdown-menu-sortable .dropdown-item').on('click', function() {
            var $this = $(this);

            if ($this.hasClass('dropdown-item-checked')) {
                $this.removeClass('dropdown-item-checked');
            } else {
                $this.closest('.dropdown-menu-sortable').find('.dropdown-item-checked').removeClass('dropdown-item-checked');
                $this.closest('.dropdown-menu-sortable').find(':radio').removeAttr('checked');
                $this.addClass('dropdown-item-checked');
                $this.find(':radio').attr('checked', true);
            }
        });

        ksInfoCancel.on('click', function () {
            ksInfo.removeClass('ks-open');
        });
    });
})(jQuery);
</script>

<script type="application/javascript">
(function ($) {
    $(document).ready(function() {
        function formatRepo (repo) {
            if (repo.loading) return repo.text;

            var markup = "<div class='ks-search-result'>" +
                "<div class='ks-avatar'><img src='" + repo.owner.avatar_url + "' /></div>" +
                "<div class='ks-meta'>" +
                "<div class='ks-title'>" + repo.full_name + "</div>";

            if (repo.description) {
                markup += "<div class='ks-description'>" + repo.description + "</div>";
            }

            markup += "<div class='ks-statistics'>" +
                "<div class='ks-forks'><i class='fa fa-flash'></i> " + repo.forks_count + " Forks</div>" +
                "<div class='ks-stargazers'><i class='fa fa-star'></i> " + repo.stargazers_count + " Stars</div>" +
                "<div class='ks-watchers'><i class='fa fa-eye'></i> " + repo.watchers_count + " Watchers</div>" +
                "</div>" +
                "</div></div>";

            return markup;
        }

        function formatRepoSelection (repo) {
            return repo.full_name || repo.text;
        }

        function formatState (state) {
            if (!state.id) {
                return state.text;
            }

            var $state = $(
                '<span class="ks-user"><img src="<?php echo assets_url; ?>img/avatars/avatar-1.jpg" class="ks-avatar" /> <span class="ks-text">' + state.text + '</span></span>'
            );

            return $state;
        }

        $('select.ks-select').select2();

        $('select.ks-select-placeholder-single').select2({
            placeholder: "Select a state",
            allowClear: true
        });

        $('select.ks-select-placeholder-multiple').select2({
            placeholder: "Select a state"
        });

        $(".ks-load-remote-data").select2({
            ajax: {
                url: "https://api.github.com/search/repositories",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function (data, params) {
                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;

                    return {
                        results: data.items,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
            minimumInputLength: 1,
            templateResult: formatRepo, // omitted for brevity, see the source of this page
            templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
        });

        $('select.ks-select-limited-number-of-selections').select2({
            maximumSelectionLength: 2
        });

        $('select.ks-select-hiding-search-box').select2({
            minimumResultsForSearch: Infinity
        });

        $('select.ks-select-tagging-support').select2({
            tags: true
        });

        $('select.ks-select-rtl-support').select2({
            dir: 'rtl'
        });

        $('select.ks-select-templating').select2({
            templateResult: formatState
        });
    });
})(jQuery);

// untuk paging
$(function(){
  $(document).on('click', ".pages_paging a",function(){  // LOAD ON PAGE LOAD AND ON CLICK
   var urls = $(this).attr("href");
   var cari = $('.cari').val();
    var sort_by = $('#sort_by').val();
     $.ajax({
        type: "post",
        url: urls,
        data: {sort_by : sort_by, keyword : cari},
        success: function(res){
                       console.log(res);
            var obj = JSON.parse(res);
        if (obj.RC == 999) {
            setContent(obj);
        }
        
        }
         
    });
      return false;
     });
});
// untuk paging
$(function(){
  $(document).on('click', ".pages_paging_pantauan a",function(){  // LOAD ON PAGE LOAD AND ON CLICK
   var urls = $(this).attr("href");
   var cari = $('.cari').val();
    var sort_by = $('#sort_by').val();
    var periode = $('#periode').val();
    var pilih_user = $('.pilih_user option:selected').val();
     $.ajax({
        type: "post",
        url: urls,
        data: {sort_by : sort_by, keyword : cari, periode : periode, pilih_user:pilih_user},
        success: function(res){
                       console.log(res);
            var obj = JSON.parse(res);
        if (obj.RC == 999) {
            setContent(obj);
        }
        
        }
         
    });
      return false;
     });
});
</script>
<script type="application/javascript">
(function ($) {
    $(document).ready(function() {
        $('.ks-daterange').daterangepicker();
        $('.ks-daterange-time').daterangepicker({
            timePicker: true,
            timePickerIncrement: 30,
            locale: {
                format: 'MM/DD/YYYY h:mm A'
            }
        });
        $('.ks-daterange-single').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true
        });

        var start = moment().subtract(29, 'days');
        var end = moment();

        $('.ks-daterange-predefined-ranges').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        });
    });
})(jQuery);


  
</script>
<div class="ks-mobile-overlay"></div>
</body>
</html>