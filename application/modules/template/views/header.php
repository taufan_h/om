<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>ORDERMATIX</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,700" rel="stylesheet">

    <!--favicon-->
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo assets_url; ?>img/ico/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo assets_url; ?>img/ico/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo assets_url; ?>img/ico/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo assets_url; ?>img/ico/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo assets_url; ?>img/icoimages/ico/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo assets_url; ?>img/ico/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo assets_url; ?>img/ico/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo assets_url; ?>img/ico/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo assets_url; ?>img/ico/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo assets_url; ?>img/ico/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo assets_url; ?>img/ico/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo assets_url; ?>img/ico/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo assets_url; ?>img/ico/favicon-16x16.png">
    <link rel="manifest" href="<?php echo assets_url; ?>img/ico/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo assets_url; ?>img/ico/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!--end favicon-->    

    <!-- CORE -->
    <link rel="stylesheet" type="text/css" href="<?php echo libs_url; ?>bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo libs_url; ?>font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo assets_url; ?>fonts/open-sans/styles.css">
    <link rel="stylesheet" type="text/css" href="<?php echo libs_url; ?>tether/css/tether.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo libs_url; ?>jscrollpane/jquery.jscrollpane.css">
    <link rel="stylesheet" type="text/css" href="<?php echo libs_url; ?>flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo assets_url; ?>styles/common.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo assets_url; ?>styles/themes/primary.min.css">    
    <!-- END CORE -->

    <!-- custom yang harus ditambah-->
    <link rel="stylesheet" type="text/css" href="<?php echo assets_url; ?>styles/apps/file-manager.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo libs_url; ?>select2/css/select2.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo assets_url; ?>styles/libs/select2/select2.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo assets_url; ?>styles/apps/messenger.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo libs_url; ?>stacktable/stacktable.css">
    <link rel="stylesheet" type="text/css" href="<?php echo assets_url; ?>styles/payment/order.min.css">
    <!--buat dashboard-->
    <link rel="stylesheet" type="text/css" href="<?php echo libs_url; ?>c3js/c3.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo assets_url; ?>styles/widgets/panels.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo assets_url; ?>styles/dashboard/mail.min.css">
    <!--end buat dashboard-->
    <!--profil-->
    <link rel="stylesheet" type="text/css" href="<?php echo assets_url; ?>styles/profile/settings.min.css">
    <!--end profil-->
    <!--date picker buat di laporan-->
    <link rel="stylesheet" type="text/css" href="<?php echo libs_url; ?>bootstrap-daterange-picker/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="<?php echo assets_url; ?>styles/libs/bootstrap-daterange-picker/daterangepicker.min.css">
    <!--end date picker buat di laporan-->

    <link rel="stylesheet" type="text/css" href="<?php echo assets_url; ?>styles/widgets/panels.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo assets_url; ?>scripts/charts/area/area.chart.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo assets_url; ?>scripts/charts/radial-progress/radial-progress.chart.min.css"><script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyByz2g9BTOKMHiC6bqi77s2-13g_2sjOmM&libraries=drawing&callback=initMap" async defer></script>
    <style>
      #map {
        width: 100%;
        height: 400px;
      }
    </style>
</head>

<body class="ks-navbar-fixed ks-sidebar-empty ks-sidebar-fixed ks-theme-primary ks-page-loading">