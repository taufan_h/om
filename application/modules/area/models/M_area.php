<?php

class M_area extends CI_Model {
    //put your code here
    public $client_id;
    public $user_id;
    public function __construct() {
        parent::__construct();

        $this->user_id = $this->session->userdata('user_id');
        $this->client_id = $this->session->userdata('client_id');
    }
    
    function select_dt( $sort_by, $offset, $limit)
    {
        $result = $this->db->query("select * FROM tbl_region  
                    inner join tbl_country using (country_id)        
                    WHERE tbl_region.client_id = '".$this->client_id."' 
                    ORDER BY ".$sort_by." asc LIMIT $offset, $limit");
        return $result;
    }
    
    function ambil_data($tbl,$field,$id)
    {
        $result = $this->db->query("select * FROM tbl_region  
                    inner join tbl_country using (country_id)        
                    WHERE tbl_region.client_id = '".$this->client_id."' 
                    && $field  = '".$id."'");
        return $result;
    }
    function select_data($table)
    {
        $this->db->select('*');
        return $this->db->get($table);
    }
    
    function total_dt()
    {
        $result = $this->db->query("select * FROM tbl_region         
                    WHERE client_id = '".$this->client_id."' ");
        return $result;
    }
    
    function cek_nama($table, $field, $record)
    {
        $this->db->select('*')
                ->where($field,$record)
                ->where('client_id',$this->client_id);
                
        $sql = $this->db->get($table);
        return $sql;
                
    }
     function cek_nama_not_in($table, $field_where,$field_not_id, $record,$user_id)
    {
        $this->db->select('*')
                ->where($field_where,$record)
                ->where_not_in($field_not_id,$user_id)
                ->where('client_id',$this->client_id);
                
        $sql = $this->db->get($table);
        return $sql;
                
    }
    function select_where($table, $field, $record)
    {
        $this->db->select('*')
                ->where($field,$record)
                ->from($table);
        return $this->db->get();
               
    }
    function insert_data($table,$data)
    {
        return $this->db->insert($table, $data);
    }
    function update_data($table,$field,$record,$data)
    {
        $this->db->where($field, $record);
        $sql = $this->db->update($table, $data); 
        return $sql;
    }

     function select_search($sort_by,$by, $offset, $limit, $keyword)
    {
        $result = $this->db->query("select * FROM tbl_region  
                    inner join tbl_country using (country_id)        
                    WHERE tbl_region.client_id = '".$this->client_id."'  && region_name like '%".$keyword."%' ORDER BY ".$sort_by." $by LIMIT $offset, $limit");
        return $result;
    }
   
    function total_search($keyword)
    {
        $result = $this->db->query("select * FROM tbl_region  
                    inner join tbl_country using (country_id)        
                    WHERE tbl_region.client_id = '".$this->client_id."'  && region_name like '%".$keyword."%'");
        return $result;
    }
   
    
}
